<?php

namespace Quiz\Tests;

use Quiz\QuizApplication;
use Quiz\Core\{Configuration, DependencyContainer};

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\MockObject;

/**
 * @coversDefaultClass \Quiz\QuizApplication
 * @uses \Quiz\QuizApplication::__construct
 * @uses \Quiz\Core\DependencyContainer
 */
class QuizApplicationTest extends TestCase
{
    /**
     * @covers ::__construct
     */
    public function testConstructor()
    {
        $app = new QuizApplication();
        self::assertInstanceOf(
            DependencyContainer::class, $app->dependencyContainer);
    }

    /**
     * @covers ::bootstrap
     * @covers ::loadConfiguration
     * @covers ::setErrorHandler
     */
    public function testBootstrap()
    {
        define('QUIZ_ROOT', 'quiz_root_canary');

        $app = new QuizApplication();
        $dc = $app->dependencyContainer = self::createMock(
            DependencyContainer::class);

        $dc->expects(self::once())
            ->method('call')
            ->with(Configuration::class, 'loadFrom',
                QUIZ_ROOT . DIRECTORY_SEPARATOR . 'config.php');

        self::assertSame($app, $app->bootstrap());

        $prevHandler = set_error_handler(null);
        self::assertNotNull($prevHandler);
    }

    public function controllerActionMapperDataProvider()
    {
        $ctrl = '\\Quiz\\Controllers\\Web\\';

        return [
            [
                [ 'GET', 'index', 'index' ],
                [ "{$ctrl}IndexController", 'getIndex' ],
            ],
            [
                [ 'POST', 'index', 'index' ],
                [ "{$ctrl}IndexController", 'postIndex' ],
            ],
            [
                [ 'GET', 'random', 'action' ],
                [ "${ctrl}RandomController", 'getAction' ],
            ],
            [
                [ 'PATCH', 'some_path', 'some_action' ],
                [ "${ctrl}SomePathController", 'patchSomeAction' ],
            ],
        ];
    }

    /**
     * @covers ::mapControllerAndAction
     * @dataProvider controllerActionMapperDataProvider
     * @uses ::Quiz\FunctionalUtilities\snake_case_to_camel_case
     */
    public function testMapControllerAndAction($args, $expected)
    {
        self::assertEquals($expected,
            QuizApplication::mapControllerAndAction(...$args));
    }

    /**
     * @covers ::setErrorHandler
     */
    public function testErrorHandler()
    {
        $app = new QuizApplication();
        $app->setErrorHandler();

        self::expectException(\ErrorException::class);
        trigger_error('some random error :)');
    }


    // I have no idea how to test dispatchRequest. That should really go into
    // its own class but unfortunately I'm not up to it right now.
    // Just a TODO for the future :)
}

