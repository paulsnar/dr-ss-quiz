<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Quiz\Tests\Services;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Quiz\Database\ORM\ModelCondition;
use Quiz\Models\UserModel;
use Quiz\Providers\SessionInterface;
use Quiz\Repositories\ObjectRepositoryInterface;
use Quiz\Services\UserService;
use Quiz\Tests\Utilities\Generators\UserGenerator;

/**
 * @coversDefaultClass \Quiz\Services\UserService
 * @covers \Quiz\Services\UserService::__construct
 * @uses \Quiz\Database\ORM\Model
 * @uses \Quiz\Database\ORM\ModelCondition
 * @uses \Quiz\Models\UserModel
 */
class UserServiceTest extends TestCase
{
    use UserGenerator;

    /** @var ObjectRepositoryInterface|MockObject */
    private $repo;

    /** @var SessionInterface|MockObject */
    private $session;

    /** @var UserService */
    private $userService;

    public function setUp()
    {
        $this->repo = self::createMock(
            ObjectRepositoryInterface::class);

        $this->session = self::createMock(
            SessionInterface::class);

        $this->userService = new UserService($this->repo, $this->session);
    }

    /**
     * @covers ::isExistingUser
     */
    public function test_givenAnExistingUser_isExistingUser_returnsTrue()
    {
        $user = $this->generateUser();

        $count = function (ModelCondition $cond) use ($user) {
            self::assertEquals(get_class($user), $cond->class);
            self::assertEquals(['id' => $user->id], $cond->conditions);
            return 1;
        };
        $this->repo->expects(self::once())
            ->method('count')
            ->willReturnCallback($count);

        self::assertTrue($this->userService->isExistingUser($user->id));
    }

    /**
     * @covers ::isExistingUser
     */
    public function test_givenANonexistentUserId_isExistingUser_returnsFalse()
    {
        $nonExistingId = 0;

        $count = function (ModelCondition $cond) use ($nonExistingId) {
            self::assertEquals(UserModel::class, $cond->class);
            self::assertEquals(['id' => $nonExistingId], $cond->conditions);
            return 0;
        };
        $this->repo->expects(self::once())
            ->method('count')
            ->willReturnCallback($count);

        self::assertFalse($this->userService->isExistingUser($nonExistingId));
    }

    /**
     * @covers ::getCurrentUser
     */
    public function test_getCurrentUser_shouldReturnCurrentUser()
    {
        $user = $this->generateUser();

        $this->session->expects(self::once())
            ->method('get')
            ->with('user_id')
            ->willReturn($user->id);

        $this->repo->expects(self::once())
            ->method('get')
            ->willReturnCallback(function (ModelCondition $cond) use ($user) {
                self::assertEquals(get_class($user), $cond->class);
                self::assertEquals(['id' => $user->id], $cond->conditions);
                return $user;
            });

        self::assertEquals($user, $this->userService->getCurrentUser());
    }

    /**
     * @covers ::getCurrentUser
     */
    public function test_givenPreviousCurrentUser_getCurrentUser_shouldCacheResults()
    {
        $user = $this->generateUser();

        $this->session->expects(self::once())
            ->method('get')
            ->with('user_id')
            ->willReturn($user->id);

        $this->repo->expects(self::once())
            ->method('get')
            ->willReturnCallback(function (ModelCondition $cond) use ($user) {
                self::assertEquals(get_class($user), $cond->class);
                self::assertEquals(['id' => $user->id], $cond->conditions);
                return $user;
            });

        $this->userService->getCurrentUser(); // warm up cache
        self::assertEquals($user, $this->userService->getCurrentUser());
    }

    /**
     * @covers ::getCurrentUser
     */
    public function test_givenNoOngoingSession_getCurrentUser_returnsNull()
    {
        $this->session->expects(self::once())
            ->method('get')
            ->with('user_id')
            ->willReturn(null);

        self::assertNull($this->userService->getCurrentUser());
    }

    /**
     * @covers ::getCurrentUser
     */
    public function test_givenADeletedUser_getCurrentUser_deletesSessionVariableAndReturnsNull()
    {
        $deletedId = 0;

        $this->session->expects(self::once())
            ->method('get')
            ->with('user_id')
            ->willReturn($deletedId);

        $get = function (ModelCondition $cond) use ($deletedId) {
            self::assertEquals(UserModel::class, $cond->class);
            self::assertEquals(['id' => $deletedId], $cond->conditions);
            return null;
        };
        $this->repo->expects(self::once())
            ->method('get')
            ->willReturnCallback($get);

        $this->session->expects(self::once())
            ->method('delete')
            ->with('user_id');

        self::assertNull($this->userService->getCurrentUser());
    }

    /**
     * @covers ::setCurrentUser
     */
    public function test_givenAnUser_setCurrentUser_setsCurrentUserToSession()
    {
        $user = $this->generateUser();

        $this->session->expects(self::once())
            ->method('set')
            ->with('user_id', $user->id);

        $this->userService->setCurrentUser($user);
    }

    /**
     * @covers ::clearCurrentUser
     */
    public function test_clearCurrentUser_clearsCurrentUser()
    {
        $this->session->expects(self::once())
            ->method('delete')
            ->with('user_id');

        $this->userService->clearCurrentUser();
    }

    /**
     * @covers ::registerUser
     */
    public function test_registerUser_savesUserToRepository()
    {
        $name = $this->generateUser()->name;

        $insert = function (UserModel $user) use ($name) {
            self::assertEquals($name, $user->name);
            return $user;
        };
        $this->repo->expects(self::once())
            ->method('insert')
            ->willReturnCallback($insert);

        $user = $this->userService->registerUser($name);
        self::assertEquals($name, $user->name);
    }
}
