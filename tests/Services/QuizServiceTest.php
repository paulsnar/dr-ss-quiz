<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Quiz\Tests\Services;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Quiz\Database\ORM\ModelCondition;
use Quiz\Exceptions\{AccessViolationException, ItemNotFoundException};
use Quiz\Models\{AnswerModel, QuestionModel, QuizModel, UserAnswerModel, UserModel};
use Quiz\Repositories\ObjectRepositoryInterface;
use Quiz\Services\{QuizService, UserService};
use Quiz\Tests\Utilities\Generators\{AnswerGenerator,
    QuestionGenerator,
    QuizGenerator,
    UserAnswerGenerator,
    UserGenerator};
use function Quiz\FunctionalUtilities\times;
use Quiz\Models\UserResultModel;

/**
 * @coversDefaultClass \Quiz\Services\QuizService
 * @covers \Quiz\Services\QuizService::__construct
 * @uses   \Quiz\Database\ORM\Model
 * @uses   \Quiz\Database\ORM\ModelCondition
 * @uses   \Quiz\Models\AnswerModel
 * @uses   \Quiz\Models\QuizModel
 * @uses   \Quiz\Models\QuestionModel
 * @uses   \Quiz\Models\UserModel
 * @uses   \Quiz\Models\UserAnswerModel
 * @uses ::Quiz\FunctionalUtilities\str_ends_with
 * @uses ::Quiz\FunctionalUtilities\times
 */
class QuizServiceTest extends TestCase
{
    use UserGenerator,
        UserAnswerGenerator,
        QuizGenerator,
        QuestionGenerator,
        AnswerGenerator;

    /** @var ObjectRepositoryInterface|MockObject */
    private $repo;

    /** @var QuizService */
    private $quizService;

    /** @var UserService|MockObject */
    private $userService;

    public function setUp()
    {
        $this->repo = self::createMock(
            ObjectRepositoryInterface::class);

        $this->userService = self::createMock(
            UserService::class);

        $this->quizService = new QuizService($this->repo, $this->userService);
    }

    /**
     * @param UserModel $user
     * @param int $times
     */
    private function setCurrentUser(UserModel $user, int $times = 1): void
    {
        $this->userService->expects(
            $times === 1 ? self::once(): self::exactly($times))
            ->method('getCurrentUser')
            ->with()
            ->willReturn($user);
    }

    /**
     * @covers ::getQuizzes
     */
    public function test_shouldReturnQuizzesFromRepository()
    {
        $quizzes = [];
        times(5, function () use (&$quizzes) {
            $quizzes[] = $this->generateQuiz();
        });
        $quizzes = new \ArrayIterator($quizzes);

        $all = function (ModelCondition $cond) use ($quizzes) {
            self::assertEquals(QuizModel::class, $cond->class);
            self::assertEquals([], $cond->conditions);
            return $quizzes;
        };
        $this->repo->expects(self::once())
            ->method('all')
            ->willReturnCallback($all);

        self::assertEquals($quizzes, $this->quizService->getQuizzes());
    }

    /**
     * @covers ::getQuestion
     */
    public function test_shouldGetQuestionById()
    {
        $question = $this->generateQuestion($this->generateQuiz());
        $question->id = rand(0, 65536);

        $get = function (ModelCondition $cond) use ($question) {
            self::assertEquals(get_class($question), $cond->class);
            self::assertEquals(['id' => $question->id], $cond->conditions);
            return $question;
        };
        $this->repo->expects(self::once())
            ->method('get')
            ->willReturnCallback($get);

        self::assertEquals($question,
            $this->quizService->getQuestion($question->id));
    }

    /**
     * @covers ::getQuestion
     */
    public function test_whenFetchingUnknownQuestion_throws()
    {
        $id = rand(0, 65536);

        $get = function (ModelCondition $cond) use ($id) {
            self::assertEquals(QuestionModel::class, $cond->class);
            self::assertEquals(['id' => $id], $cond->conditions);
            return null;
        };
        $this->repo->expects(self::once())
            ->method('get')
            ->willReturnCallback($get);

        self::expectException(ItemNotFoundException::class);
        $this->quizService->getQuestion($id);
    }

    /**
     * @covers ::getQuestions
     */
    public function test_shouldGetQuestionsForQuiz()
    {
        $quiz = $this->generateQuiz();

        $questions = [];
        times(5, function () use ($quiz, &$questions) {
            $questions[] = $this->generateQuestion($quiz);
        });
        $questions = new \ArrayIterator($questions);

        $all = function (ModelCondition $cond) use ($quiz, $questions) {
            self::assertEquals(QuestionModel::class, $cond->class);
            self::assertEquals(['quiz_id' => $quiz->id], $cond->conditions);
            return $questions;
        };
        $this->repo->expects(self::once())
            ->method('all')
            ->willReturnCallback($all);

        self::assertEquals($questions,
            $this->quizService->getQuestions($quiz->id));
    }

    /**
     * @covers ::getAnswers
     */
    public function test_shouldGetAnswersForQuestion()
    {
        $question = $this->generateQuestion($this->generateQuiz());

        $answers = [];
        times(10, function () use ($question, &$answers) {
            $answers[] = $this->generateAnswer($question);
        });
        $answers = new \ArrayIterator($answers);

        $all = function (ModelCondition $cond) use ($question, $answers) {
            self::assertEquals(AnswerModel::class, $cond->class);
            self::assertEquals(
                ['question_id' => $question->id], $cond->conditions);
            return $answers;
        };
        $this->repo->expects(self::once())
            ->method('all')
            ->willReturnCallback($all);

        self::assertEquals($answers,
            $this->quizService->getAnswers($question->id));
    }

    /**
     * @covers ::isQuizCompleted
     */
    public function test_givenNoOngoingSession_isQuizCompleted_throws()
    {
        $this->userService->expects(self::once())
            ->method('getCurrentUser')
            ->willReturn(null);

        self::expectException(ItemNotFoundException::class);
        $this->quizService->isQuizCompleted(0);
    }

    /**
     * @covers ::isQuizCompleted
     */
    public function test_givenIncompleteAttempt_isQuizCompleted_returnsFalse()
    {
        $quiz = $this->generateQuiz();
        $user = $this->generateUser();
        $this->setCurrentUser($user);

        $count = function (ModelCondition $cond) use ($quiz, $user) {
            if ($cond->class === QuestionModel::class) {
                self::assertEquals(['quiz_id' => $quiz->id], $cond->conditions);
                return 5;
            } else if ($cond->class === UserAnswerModel::class) {
                self::assertEquals(
                    ['quiz_id' => $quiz->id, 'user_id' => $user->id],
                    $cond->conditions
                );
                return 3;
            } else {
                self::fail("Tried to count unexpected model {$cond->class}");
            }
        };

        $this->repo->expects(self::exactly(2))
            ->method('count')
            ->willReturnCallback($count);

        self::assertFalse($this->quizService->isQuizCompleted($quiz->id));
    }

    /**
     * @covers ::isQuizCompleted
     */
    public function test_givenFinishedAttempt_isQuizCompleted_returnsTrue()
    {
        $quiz = $this->generateQuiz();
        $user = $this->generateUser();
        $this->setCurrentUser($user);

        $count = function (ModelCondition $cond) use ($quiz, $user) {
            if ($cond->class === QuestionModel::class) {
                self::assertEquals(['quiz_id' => $quiz->id], $cond->conditions);
                return 5;
            } else if ($cond->class === UserAnswerModel::class) {
                self::assertEquals(
                    ['quiz_id' => $quiz->id, 'user_id' => $user->id],
                    $cond->conditions
                );
                return 5;
            } else {
                self::fail("Tried to count unexpected model {$cond->class}");
            }
        };
        $this->repo->expects(self::exactly(2))
            ->method('count')
            ->willReturnCallback($count);

        self::assertTrue($this->quizService->isQuizCompleted($quiz->id));
    }

    /**
     * @covers ::getUserAnswers
     */
    public function test_shouldGetSubmittedUserAnswers()
    {
        $quiz = $this->generateQuiz();
        $user = $this->generateUser();
        $this->setCurrentUser($user);

        $userAnswers = [];
        times(20, function () use (&$userAnswers, $quiz, $user) {
            $question = $this->generateQuestion($quiz);
            $answer = $this->generateAnswer($question);
            $userAnswers[] = $this->generateUserAnswer($user, $answer);
        });
        $userAnswers = new \ArrayIterator($userAnswers);

        $all = function (ModelCondition $cond)
        use ($user, $quiz, $userAnswers) {
            self::assertEquals(UserAnswerModel::class, $cond->class);
            self::assertEquals(
                ['user_id' => $user->id, 'quiz_id' => $quiz->id],
                $cond->conditions
            );
            return $userAnswers;
        };
        $this->repo->expects(self::once())
            ->method('all')
            ->willReturnCallback($all);

        self::assertEquals($userAnswers,
            $this->quizService->getUserAnswers($quiz->id));
    }

    /**
     * @covers ::getUserAnswers
     */
    public function test_givenNoOngoingSession_getUserAnswers_throws()
    {
        $this->userService->expects(self::once())
            ->method('getCurrentUser')
            ->willReturn(null);

        self::expectException(ItemNotFoundException::class);
        $this->quizService->getUserAnswers(0);
    }

    /**
     * @depends test_shouldGetSubmittedUserAnswers
     * @uses    \Quiz\Services\QuizService::getUserAnswers
     * @covers ::getCorrectAndTotalAnswerCount
     * @covers ::getScore
     */
    public function test_givenAnAttempt_getCorrectAndTotalAnswerCount_shouldReturnCorrectValues_and_getScore_shouldReturnCorrectScore()
    {
        // I can't even be bothered to replicate all of this to test the score
        // so we just do it here as a by-the-way.

        $quiz = $this->generateQuiz();
        $user = $this->generateUser();
        $this->setCurrentUser($user, 2);

        $total = rand(20, 50);
        $correct = rand(0, $total);

        $userAnswers = [];
        $answers = [];
        times($total, function () use (
            &$userAnswers,
            &$answers,
            $user,
            $quiz
        ) {
            $question = $this->generateQuestion($quiz);
            $answer = $this->generateAnswer($question);
            $answers[$answer->id] = $answer;
            $answer->isCorrect = false;

            $userAnswers[] = $this->generateUserAnswer($user, $answer);
        });

        $haveCorrect = 0;
        foreach ($answers as $answer) {
            if ($haveCorrect === $correct) {
                break;
            }

            $answer->isCorrect = true;
            $haveCorrect += 1;
        }

        $userAnswers = new \ArrayIterator($userAnswers);

        $all = function (ModelCondition $cond)
        use ($user, $quiz, $userAnswers) {
            self::assertEquals(UserAnswerModel::class, $cond->class);
            self::assertEquals(
                ['user_id' => $user->id, 'quiz_id' => $quiz->id],
                $cond->conditions
            );
            return $userAnswers;
        };
        $this->repo->expects(self::exactly(2))
            ->method('all')
            ->willReturnCallback($all);

        $get = function (ModelCondition $cond) use ($answers) {
            self::assertEquals(AnswerModel::class, $cond->class);
            return $answers[$cond->conditions['id']];
        };
        $this->repo->expects(self::any())
            ->method('get')
            ->willReturnCallback($get);

        self::assertEquals([$correct, $total],
            $this->quizService->getCorrectAndTotalAnswerCount($quiz->id));

        self::assertEquals((int)round(100 * $correct / $total),
            $this->quizService->getScore($quiz->id));
    }

    /**
     * @covers ::submitAnswer
     */
    public function test_givenANewAnswer_submitAnswer_shouldSaveAnswer()
    {
        $quiz = $this->generateQuiz();
        $user = $this->generateUser();
        $this->setCurrentUser($user);

        $question = $this->generateQuestion($quiz);
        $answer = $this->generateAnswer($question);

        $count = function (ModelCondition $cond) use ($user, $question, $answer) {
            if ($cond->class === AnswerModel::class) {
                self::assertEquals(
                    ['id' => $answer->id, 'question_id' => $question->id],
                    $cond->conditions
                );
                return 1;
            } else if ($cond->class === UserAnswerModel::class) {
                self::assertEquals(
                    ['user_id' => $user->id, 'question_id' => $question->id],
                    $cond->conditions
                );
                return 0;
            } else {
                self::fail("Tried to count unexpected model {$cond->class}");
            }
        };
        $this->repo->expects(self::exactly(2))
            ->method('count')
            ->willReturnCallback($count);

        $get = function (ModelCondition $cond) use ($question) {
            self::assertEquals(QuestionModel::class, $cond->class);
            self::assertEquals(['id' => $question->id], $cond->conditions);
            return $question;
        };
        $this->repo->expects(self::once())
            ->method('get')
            ->willReturnCallback($get);

        $this->repo->expects(self::once())
            ->method('insert')
            ->willReturnArgument(0);

        $userAnswer = $this->quizService->submitAnswer(
            $quiz->id, $question->id, $answer->id);

        self::assertEquals($user, $userAnswer->user);
        self::assertEquals([
            'quiz' => $quiz->id,
            'question' => $question->id,
            'answer' => $answer->id,
        ], $userAnswer->_stubReferences);
    }

    /**
     * @covers ::submitAnswer
     */
    public function test_givenAnInvalidAnswer_submitAnswer_throws()
    {
        $answer = $this->generateAnswer(
            $this->generateQuestion($this->generateQuiz()));

        $user = $this->generateUser();
        $this->setCurrentUser($user);

        $count = function (ModelCondition $cond) use ($answer) {
            self::assertEquals(AnswerModel::class, $cond->class);
            self::assertEquals(
                ['id' => $answer->id, 'question_id' => $answer->question->id],
                $cond->conditions
            );
            return 0;
        };
        $this->repo->expects(self::once())
            ->method('count')
            ->willReturnCallback($count);

        self::expectException(ItemNotFoundException::class);
        $this->quizService->submitAnswer($answer->question->quiz->id,
            $answer->question->id, $answer->id);
    }

    /**
     * @covers ::submitAnswer
     */
    public function test_givenADuplicateAnswer_submitAnswer_throws()
    {
        $answer = $this->generateAnswer(
            $this->generateQuestion($this->generateQuiz()));

        $user = $this->generateUser();
        $this->setCurrentUser($user);

        $count = function (ModelCondition $cond) use ($user, $answer) {
            if ($cond->class === AnswerModel::class) {
                self::assertEquals(['id' => $answer->id,
                    'question_id' => $answer->question->id], $cond->conditions);
                return 1;
            } else if ($cond->class === UserAnswerModel::class) {
                self::assertEquals(['user_id' => $user->id,
                    'question_id' => $answer->question->id], $cond->conditions);
                return 1;
            } else {
                self::fail("Tried to count unexpected model {$cond->class}");
            }
        };
        $this->repo->expects(self::exactly(2))
            ->method('count')
            ->willReturnCallback($count);

        self::expectException(AccessViolationException::class);
        $this->quizService->submitAnswer($answer->question->quiz->id,
            $answer->question->id, $answer->id);
    }

    /**
     * @covers ::submitAnswer
     */
    public function test_givenNoOngoingSession_submitAnswer_throws()
    {
        $this->userService->expects(self::once())
            ->method('getCurrentUser')
            ->willReturn(null);

        self::expectException(ItemNotFoundException::class);
        $this->quizService->submitAnswer(0, 0, 0);
    }

    /**
     * @covers ::finishAttempt
     */
    public function test_givenNoOngoingSession_finishAttempt_throws()
    {
        $this->userService->expects(self::once())
            ->method('getCurrentUser')
            ->willReturn(null);

        self::expectException(ItemNotFoundException::class);
        $this->quizService->finishAttempt(0);
    }

    /**
     * @covers ::finishAttempt
     */
    public function test_givenOngoingAttempt_finishAttempt_savesUserResult()
    {
        // Okay I can't be bothered to set up the fixture for ->getScore to
        // return predefined results so instead we partially mock it here.

        $user = $this->generateUser();
        $quiz = $this->generateQuiz();

        $this->userService->expects(self::once())
            ->method('getCurrentUser')
            ->willReturn($user);

        $this->repo->expects(self::once())
            ->method('insert')
            ->willReturnCallback(
            function (UserResultModel $userResult) use ($quiz, $user) {
                self::assertEquals($userResult->quizId, $quiz->id);
                self::assertEquals($userResult->userId, $user->id);
                self::assertEquals(42, $userResult->score);
                return $userResult;
            });

        $quizzes = self::getMockBuilder(QuizService::class)
            ->setMethods(['getScore'])
            ->setConstructorArgs([$this->repo, $this->userService])
            ->getMock();

        $quizzes->expects(self::once())
            ->method('getScore')
            ->willReturn(42);

        $quizzes->finishAttempt($quiz->id);
    }
}
