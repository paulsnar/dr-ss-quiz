<?php

namespace Quiz\Tests\HTTP;

use Quiz\HTTP\ImmutableHeaderBag;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Quiz\HTTP\ImmutableHeaderBag
 * @uses \Quiz\HTTP\ImmutableHeaderBag::__construct
 * @uses \Quiz\HTTP\HeaderBag
 * @uses \Quiz\Utilities\Structs\Bag
 */
class ImmutableHeaderBagTest extends TestCase
{
    /**
     * @covers ::__construct
     * @covers ::contains
     * @covers ::get
     */
    public function testImmutableOperations()
    {
        $bag = new ImmutableHeaderBag([
            'Some-Header' => 'forty two',
            'other-header' => '42',
        ]);

        self::assertTrue($bag->contains('some-header'));
        self::assertTrue($bag->contains('Other-Header'));
        self::assertFalse($bag->contains('Nonexistent'));

        self::assertEquals('forty two', $bag->get('Some-HEADER'));
        self::assertEquals('42', $bag->get('OTHER-HEADER'));
        self::assertNull($bag->get('nonexistent'));
    }

    /**
     * @covers ::__construct
     * @covers ::set
     * @covers ::unset
     */
    public function test_mutableOperations_throw()
    {
        $bag = new ImmutableHeaderBag([ 'forty-two' => '42' ]);

        try {
            $bag->set('other-Headers', 'are not that good');
        } catch (\LogicException $e) {
            self::assertTrue(true);
        }

        try {
            $bag->unset('Forty-Two');
        } catch (\LogicException $e) {
            self::assertTrue(true);
        }
    }

    /**
     * @covers ::fromGlobals
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function test_fromGlobals_createsImmutably()
    {
        $bag = ImmutableHeaderBag::fromGlobals();
        self::assertInstanceOf(ImmutableHeaderBag::class, $bag);
    }
}
