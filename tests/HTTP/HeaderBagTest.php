<?php

namespace Quiz\Tests\HTTP;

use Quiz\HTTP\HeaderBag;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Quiz\HTTP\HeaderBag
 * @uses \Quiz\HTTP\HeaderBag::__construct
 * @uses \Quiz\Utilities\Structs\Bag
 */
class HeaderBagTest extends TestCase
{
    /** @var HeaderBag */
    private $bag;

    public function setUp()
    {
        $this->bag = new HeaderBag([
            'UPPER-CASE' => 'upper',
            'lower-case' => 'lower',
            'Mixed-Case' => 'mixed',
            'Multi-Valued' => [ 'a', 'b' ],
        ]);
    }

    /**
     * @covers ::__construct
     */
    public function testConstructor()
    {
        self::assertTrue(true); // did not throw on setUp
    }

    /**
     * @covers ::contains
     */
    public function testContains()
    {
        self::assertTrue($this->bag->contains('Upper-Case'));
        self::assertTrue($this->bag->contains('LOWER-CASE'));
        self::assertTrue($this->bag->contains('mixed-Case'));
        self::assertTrue($this->bag->contains('Multi-valued'));

        self::assertFalse($this->bag->contains('upper_case'));
        self::assertFalse($this->bag->contains('LowerCase'));
    }

    /**
     * @covers ::get
     */
    public function testGet()
    {
        self::assertEquals('upper', $this->bag->get('upper-case'));
        self::assertEquals('lower', $this->bag->get('Lower-Case'));
        self::assertEquals('mixed', $this->bag->get('MIXED-CASE'));
        self::assertEquals(['a', 'b'], $this->bag->get('multi-valued'));

        self::assertNull($this->bag->get('nonexistent'));
    }

    /**
     * @covers ::set
     * @uses \Quiz\HTTP\HeaderBag::get
     * @depends testGet
     */
    public function testSet()
    {
        $this->bag->set('UPPER-CASE-TWO', 'upper2');
        $this->bag->set('lower-case-two', 'lower2');
        $this->bag->set('Mixed-Case-Two', 'mixed2');
        $this->bag->set('multi-Valued-TWO', ['a', 'b', 'c']);

        self::assertEquals('upper2', $this->bag->get('upper-Case-TWO'));
        self::assertEquals('lower2', $this->bag->get('LOWER-Case-two'));
        self::assertEquals('mixed2', $this->bag->get('MIXED-CASE-TWO'));
        self::assertEquals(['a', 'b', 'c'],
            $this->bag->get('multi-valued-two'));
    }

    /**
     * @covers ::unset
     * @uses \Quiz\HTTP\HeaderBag::contains
     * @depends testContains
     */
    public function testUnset()
    {
        $this->bag->unset('upper-case');
        $this->bag->unset('Lower-Case');
        $this->bag->unset('MIXED-cASE');
        $this->bag->unset('multi-VALUED');

        self::assertFalse($this->bag->contains('Upper-Case'));
        self::assertFalse($this->bag->contains('LOWER-case'));
        self::assertFalse($this->bag->contains('mixed-case'));
        self::assertFalse($this->bag->contains('multi-Valued'));
    }

    /**
     * @backupGlobals enabled
     * @covers ::fromGlobals
     * @uses \Quiz\HTTP\HeaderBag::contains
     * @uses \Quiz\HTTP\HeaderBag::get
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function testFromGlobals()
    {
        $canary = bin2hex(random_bytes(4));
        $_SERVER['HTTP_SOME_HEADER'] = 'some value';
        $_SERVER["HTTP_CANARY_{$canary}"] = 'some other value';

        $headers = HeaderBag::fromGlobals();

        self::assertTrue($headers->contains('some-Header'));
        self::assertTrue($headers->contains("Canary-{$canary}"));

        self::assertEquals(
            $_SERVER['HTTP_SOME_HEADER'], $headers->get('some-header'));
        self::assertEquals($_SERVER["HTTP_CANARY_{$canary}"],
            $headers->get("CANARY-{$canary}"));
    }
}
