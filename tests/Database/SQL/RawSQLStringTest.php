<?php


namespace Quiz\Tests\Database\SQL;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\EscaperInterface;
use Quiz\Database\SQL\RawSQLString;

class RawSQLStringTest extends TestCase
{
    public function test_shouldPassThroughValue()
    {
        $nonce = '';
        for ($i = 0; $i < 256; $i += 1) {
            $nonce .= chr($i);
        }

        $str = new RawSQLString($nonce);

        /** @var EscaperInterface|MockObject $serializer */
        $serializer = self::createMock(EscaperInterface::class);
        self::assertEquals($nonce, $str->serialize($serializer));
    }
}
