<?php


namespace Quiz\Tests\Database\SQL;


use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\MySQLEscaper;
use Quiz\Database\SQL\SQLSerializableInterface;

/**
 * @coversDefaultClass \Quiz\Database\SQL\MySQLEscaper
 */
class MySQLEscaperTest extends TestCase
{
    /** @var MySQLEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new MySQLEscaper();
    }

    public function identifierEscapingProvider()
    {
        return [
            [ 'a', '`a`' ],
            [ 'some string with spaces', '`some string with spaces`' ],
            [ 'a`backtick', '`a``backtick`' ],
        ];
    }

    /**
     * @covers ::identifier
     * @dataProvider identifierEscapingProvider
     */
    public function testIdentifierEscaping($identifier, $expected)
    {
        self::assertEquals($expected, $this->escaper->identifier($identifier));
    }


    public function valueEscapingProvider()
    {
        return [
            [ 'a', 'a' ],
            [ true, '1' ],
            [ false, '0' ],
            [
                new \DateTime('1970-01-01T00:00:00Z'),
                '1970-01-01 00:00:00',
            ],
            [
                new \DateTimeImmutable('1970-01-01T00:00:00Z'),
                '1970-01-01 00:00:00',
            ],
        ];
    }

    /**
     * @covers ::value
     * @dataProvider valueEscapingProvider
     */
    public function testValueEscaping($value, $expected)
    {
        self::assertEquals($expected, $this->escaper->value($value));
    }

    /**
     * @covers ::identifier
     */
    public function testIdentifierEscapingWithSerializable()
    {
        $ident = self::createMock(
            SQLSerializableInterface::class);
        $ident->expects(self::once())
            ->method('serialize')
            ->with($this->escaper)
            ->willReturn('identifier');

        self::assertEquals('identifier',
            $this->escaper->identifier($ident));
    }

    /**
     * @covers ::value
     */
    public function testValueEscapingWithSerializable()
    {
        $value = self::createMock(
            SQLSerializableInterface::class);
        $value->expects(self::once())
            ->method('serialize')
            ->with($this->escaper)
            ->willReturn('value');

        self::assertEquals('value',
            $this->escaper->value($value));
    }
}
