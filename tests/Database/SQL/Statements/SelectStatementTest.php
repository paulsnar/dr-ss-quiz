<?php


namespace Quiz\Tests\Database\SQL\Statements;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\PassthroughEscaper;
use Quiz\Database\SQL\RawSQLString;
use Quiz\Database\SQL\SQLParameterCollectorInterface;
use Quiz\Database\SQL\Statements\SelectStatement;

/**
 * @coversDefaultClass \Quiz\Database\SQL\Statements\SelectStatement
 * @covers \Quiz\Database\SQL\Statements\SelectStatement::__construct
 * @uses \Quiz\Database\SQL\RawSQLString
 * @uses \Quiz\Database\SQL\PassthroughEscaper
 */
class SelectStatementTest extends TestCase
{
    /** @var PassthroughEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new PassthroughEscaper();
    }

    /**
     * @covers ::serialize
     */
    public function test_givenNoConditions_shouldGenerateSelectKeyword()
    {
        $select = new SelectStatement();
        $select->columns = null;
        self::assertEquals('SELECT',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenOnlyColumnNames_shouldGenerateCorrectSQL()
    {
        $select = new SelectStatement();
        $select->columns = ['a', 'b', 'c'];

        self::assertEquals('SELECT a, b, c',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenSerializableAsColumnDefinition_shouldGenerateCorrectSQL()
    {
        $select = new SelectStatement();
        $select->columns = new RawSQLString('this stuff');

        self::assertEquals('SELECT this stuff',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenNoColumnsExplicitly_shouldGenerateSelectAsterisk()
    {
        $select = new SelectStatement();
        self::assertEquals('SELECT *',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenColumnsAsAssociativeArray_shouldGenerateColumnAliases()
    {
        $select = new SelectStatement();
        $select->columns = [
            'a' => 'ayy',
            'b' => 'bee',
            'c' => 'cee',
        ];

        self::assertEquals('SELECT a AS ayy, b AS bee, c AS cee',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenTableName_shouldGenerateFromClause()
    {
        $select = new SelectStatement();
        $select->columns = null;
        $select->table = 'table';
        self::assertEquals('SELECT FROM table',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenWhereClause_shouldAddItAsSerialized()
    {
        $select = new SelectStatement();
        $select->columns = null;
        $select->where = new RawSQLString('this and that');
        self::assertEquals('SELECT WHERE this and that',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenLimitClause_shouldAddItAsSerialized()
    {
        $select = new SelectStatement();
        $select->columns = null;
        $select->limit = 42;
        self::assertEquals('SELECT LIMIT 42',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenOffsetClause_shouldAddItAsSerialized()
    {
        $select = new SelectStatement();
        $select->columns = null;
        $select->offset = 42;
        self::assertEquals('SELECT OFFSET 42',
            $select->serialize($this->escaper));
    }

    /**
     * @covers ::collectParameters
     */
    public function test_givenNoWhereClause_shouldCollectNoParameters()
    {
        $select = new SelectStatement();
        self::assertEquals([], $select->collectParameters($this->escaper));
    }

    /**
     * @covers ::collectParameters
     */
    public function test_givenWhereClause_forwardsItsParameters()
    {
        $params = [ 1, 2, 3, 4, 5 ];

        $where = self::createMock(
            SQLParameterCollectorInterface::class);

        $where->expects(self::once())
            ->method('collectParameters')
            ->with($this->escaper)
            ->willReturn($params);

        $select = new SelectStatement();
        $select->where = $where;

        self::assertEquals($params, $select->collectParameters($this->escaper));
    }
    
    /**
     * @covers ::__construct
     */
    public function testFluentInterface_zeroArgConstructor_setsColumnsToEmpty()
    {
        $select = new SelectStatement();
        self::assertEquals([], $select->columns);
    }

    /**
     * @covers ::__construct
     */
    public function testFluentInterface_constructorWithStringArgs_setsColumnsToArgs()
    {
        $cols = [ 'a', 'b', 'c' ];
        $select = new SelectStatement(...$cols);
        self::assertEquals($cols, $select->columns);
    }

    /**
     * @covers ::__construct
     */
    public function testFluentInterface_constructorWithSingleArrayArg_setsColumnsToArg()
    {
        $cols = [ 'a', 'b', 'c' ];
        $select = new SelectStatement($cols);
        self::assertEquals($cols, $select->columns);
    }

    /**
     * @covers ::from
     */
    public function testFluentInterface_fromMethod_setsTable()
    {
        $table = 'some random table I dunno';
        $select = (new SelectStatement())->from($table);
        self::assertEquals($table, $select->table);
    }

    /**
     * @covers ::where
     */
    public function testFluentInterface_whereMethod_setsWhere()
    {
        /** @var ConditionTree|MockObject $where */
        $where = self::createMock(ConditionTree::class);
        $select = (new SelectStatement())->where($where);
        self::assertSame($where, $select->where);
    }
}
