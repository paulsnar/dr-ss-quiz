<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Quiz\Tests\Database\SQL\Statements;


use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\PassthroughEscaper;
use Quiz\Database\SQL\Statements\UpdateStatement;

/**
 * @coversDefaultClass \Quiz\Database\SQL\Statements\UpdateStatement
 * @covers \Quiz\Database\SQL\Statements\UpdateStatement::__construct
 * @uses \Quiz\Database\SQL\PassthroughEscaper
 */
class UpdateStatementTest extends TestCase
{
    /** @var PassthroughEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new PassthroughEscaper();
    }

    /**
     * @covers ::__construct
     */
    public function testFluentInterface_constructorShouldSetTable()
    {
        $table = random_bytes(16);
        $update = new UpdateStatement($table);
        self::assertEquals($table, $update->table);
    }

    /**
     * @covers ::set
     */
    public function testFluentInterface_setMethod_shouldAddColumnAndValue()
    {
        $update = new UpdateStatement('some table');
        $update->set('column', 'value');
        self::assertEquals(['column'], $update->columns);
        self::assertEquals(['value'], $update->values);

        $update->set('a', '1');
        $update->set('b', '2');
        $update->set('c', '3');
        self::assertEquals(['column', 'a', 'b', 'c'], $update->columns);
        self::assertEquals(['value', '1', '2', '3'], $update->values);
    }

    /**
     * @covers ::serialize
     * @depends testFluentInterface_constructorShouldSetTable
     */
    public function test_givenTableAndColumns_shouldGenerateSQL()
    {
        $update = new UpdateStatement('some table');
        $update->columns = ['a', 'b', 'c'];
        $update->values = ['dummy', 'dummy', 'dummy'];
        self::assertEquals(
            'UPDATE some table SET a = ?, b = ?, c = ?',
            $update->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenWhereClause_shouldSerializeIt()
    {
        $where = self::createMock(ConditionTree::class);
        $where->expects(self::once())
            ->method('serialize')
            ->with($this->escaper)
            ->willReturn('some where clause');

        $update = new UpdateStatement('some table');
        $update->columns = ['a'];
        $update->where = $where;

        self::assertEquals(
            'UPDATE some table SET a = ? WHERE some where clause',
            $update->serialize($this->escaper));
    }

    /**
     * @covers ::collectParameters
     */
    public function test_givenNoWhereClause_shouldReturnValuesAsParameters()
    {
        $params = ['a', 'b', 'c'];
        $update = new UpdateStatement('some table');
        $update->columns = ['dummy', 'dummy', 'dummy'];
        $update->values = $params;

        self::assertEquals(
            $params, $update->collectParameters($this->escaper));
    }

    /**
     * @covers ::collectParameters
     */
    public function test_givenWhereClause_shouldMergeWhereAndOwnParameters()
    {
        $params = ['a', 'b', 'c'];
        $whereParams = ['d', 'e', 'f'];

        $where = self::createMock(ConditionTree::class);

        $where->expects(self::once())
            ->method('collectParameters')
            ->with($this->escaper)
            ->willReturn($whereParams);

        $update = new UpdateStatement('some table');
        $update->columns = ['dummy', 'dummy', 'dummy'];
        $update->values = $params;
        $update->where = $where;

        self::assertEquals(
            array_merge($params, $whereParams),
            $update->collectParameters($this->escaper));

    }
}
