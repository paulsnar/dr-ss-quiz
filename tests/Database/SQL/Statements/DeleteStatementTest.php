<?php


namespace Quiz\Tests\Database\SQL\Statements;


use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\PassthroughEscaper;
use Quiz\Database\SQL\Statements\DeleteStatement;

/**
 * @coversDefaultClass \Quiz\Database\SQL\Statements\DeleteStatement
 * @covers \Quiz\Database\SQL\Statements\DeleteStatement::__construct
 * @uses \Quiz\Database\SQL\PassthroughEscaper
 */
class DeleteStatementTest extends TestCase
{
    /** @var PassthroughEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new PassthroughEscaper();
    }

    /**
     * @covers ::__construct
     */
    public function testFluentInterface_constructor_setsTable()
    {
        $delete = new DeleteStatement('some table');
        static::assertEquals('some table', $delete->table);
    }

    /**
     * @depends testFluentInterface_constructor_setsTable
     * @covers ::serialize
     */
    public function test_givenNoWhereClause_generatesSQL()
    {
        $delete = new DeleteStatement('some table');
        self::assertEquals('DELETE FROM some table',
            $delete->serialize($this->escaper));
    }

    /**
     * @depends testFluentInterface_constructor_setsTable
     * @covers ::serialize
     */
    public function test_givenWhereClause_generatesSQL()
    {
        $where = self::createMock(ConditionTree::class);
        $where->expects(self::once())
            ->method('serialize')
            ->with($this->escaper)
            ->willReturn('some where clause');

        $delete = new DeleteStatement('some table');
        $delete->where = $where;

        self::assertEquals(
            'DELETE FROM some table WHERE some where clause',
            $delete->serialize($this->escaper));
    }

    /**
     * @depends testFluentInterface_constructor_setsTable
     * @covers ::collectParameters
     */
    public function test_givenNoWhereClause_collectParameters_returnsEmptyArray()
    {
        $delete = new DeleteStatement('some table');
        self::assertEquals([], $delete->collectParameters($this->escaper));
    }

    /**
     * @depends testFluentInterface_constructor_setsTable
     * @covers ::collectParameters
     */
    public function test_givenWhereClause_collectParameters_forwardsParameters()
    {
        $params = ['a', 'b', 'c'];

        $where = self::createMock(ConditionTree::class);
        $where->expects(self::once())
            ->method('collectParameters')
            ->with($this->escaper)
            ->willReturn($params);

        $delete = new DeleteStatement('some table');
        $delete->where = $where;

        self::assertEquals($params,
            $delete->collectParameters($this->escaper));
    }
}
