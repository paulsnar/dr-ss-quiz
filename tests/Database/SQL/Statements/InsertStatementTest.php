<?php


namespace Quiz\Tests\Database\SQL\Statements;


use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\PassthroughEscaper;
use Quiz\Database\SQL\Statements\InsertStatement;

/**
 * @coversDefaultClass \Quiz\Database\SQL\Statements\InsertStatement
 * @uses \Quiz\Database\SQL\PassthroughEscaper
 */
class InsertStatementTest extends TestCase
{
    /** @var PassthroughEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new PassthroughEscaper();
    }

    /**
     * @covers ::into
     */
    public function testFluentInterface_intoStaticMethod_shouldSetTable()
    {
        $insert = InsertStatement::into('some table');
        self::assertEquals('some table', $insert->table);
    }

    /**
     * @depends testFluentInterface_intoStaticMethod_shouldSetTable
     * @covers ::serialize
     * @uses \Quiz\Database\SQL\Statements\InsertStatement::into
     */
    public function test_givenTableAndValues_shouldGenerateSQL()
    {
        $insert = InsertStatement::into('some table');
        $insert->values = ['dummy', 'dummy', 'dummy'];
        self::assertEquals('INSERT INTO some table VALUES (?, ?, ?)',
            $insert->serialize($this->escaper));
    }

    /**
     * @depends testFluentInterface_intoStaticMethod_shouldSetTable
     * @covers ::serialize
     * @uses \Quiz\Database\SQL\Statements\InsertStatement::into
     */
    public function test_givenColumnSpecifier_shouldGenerateSQL()
    {
        $insert = InsertStatement::into('some table');
        $insert->columns = ['a', 'b', 'c'];
        $insert->values = ['dummy', 'dummy', 'dummy'];
        self::assertEquals(
            'INSERT INTO some table (a, b, c) VALUES (?, ?, ?)',
            $insert->serialize($this->escaper));
    }

    /**
     * @covers ::collectParameters
     */
    public function test_givenValues_returnsThemAsParameters()
    {
        $insert = new InsertStatement();
        $values = ['a', 'b', 'c'];
        $insert->values = $values;
        self::assertEquals($values,
            $insert->collectParameters($this->escaper));
    }
}
