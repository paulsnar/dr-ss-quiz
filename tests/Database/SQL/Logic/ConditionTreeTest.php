<?php


namespace Quiz\Tests\Database\SQL\Logic;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\Logic\{ConditionTree, WhereClause};
use Quiz\Database\SQL\PassthroughEscaper;
use Quiz\Database\SQL\RawSQLString;

class ConditionTreeTest extends TestCase
{
    /** @var PassthroughEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new PassthroughEscaper();
    }

    public function testSerialize_givenNoConditions_shouldReturnEmptyString()
    {
        $ctAnd = new ConditionTree(ConditionTree::TYPE_AND);
        self::assertEquals('', $ctAnd->serialize($this->escaper));
        $ctOr = new ConditionTree(ConditionTree::TYPE_OR);
        self::assertEquals('', $ctOr->serialize($this->escaper));
    }

    public function testSerialize_givenSingleCondition_shouldReturnConditionRegardlessOfType()
    {
        $ctAnd = new ConditionTree(ConditionTree::TYPE_AND);
        $ctAnd->add(new RawSQLString('condition'));
        self::assertEquals(
            '(condition)', $ctAnd->serialize($this->escaper));

        $ctOr = new ConditionTree(ConditionTree::TYPE_OR);
        $ctOr->add(new RawSQLString('condition'));
        self::assertEquals(
            '(condition)', $ctOr->serialize($this->escaper));
    }

    public function test_fluentInterfaces()
    {
        $a = new RawSQLString('a');

        $ctAndFluent = ConditionTree::and([$a, $a, $a]);
        $ctAndTraditional = new ConditionTree(ConditionTree::TYPE_AND);
        $ctAndTraditional->add($a);
        $ctAndTraditional->add($a);
        $ctAndTraditional->add($a);
        self::assertEquals($ctAndTraditional, $ctAndFluent);

        $ctOrFluent = ConditionTree::or([$a, $a, $a]);
        $ctOrTraditional = new ConditionTree(ConditionTree::TYPE_OR);
        $ctOrTraditional->add($a);
        $ctOrTraditional->add($a);
        $ctOrTraditional->add($a);
        self::assertEquals($ctOrTraditional, $ctOrFluent);

        $ctAndFluent = ConditionTree::and([ 'a' => 'b', 'c' => 'd' ]);
        $ctAndTraditional = new ConditionTree(ConditionTree::TYPE_AND);
        $ctAndTraditional->add(WhereClause::equals('a', 'b'));
        $ctAndTraditional->add(WhereClause::equals('c', 'd'));
        self::assertEquals($ctAndTraditional, $ctAndFluent);

        $ctOrFluent = ConditionTree::or([ 'a' => 'b', 'c' => 'd' ]);
        $ctOrTraditional = new ConditionTree(ConditionTree::TYPE_OR);
        $ctOrTraditional->add(WhereClause::equals('a', 'b'));
        $ctOrTraditional->add(WhereClause::equals('c', 'd'));
        self::assertEquals($ctOrTraditional, $ctOrFluent);
    }

    /**
     * @depends test_fluentInterfaces
     */
    public function testSerialize_givenMultipleConditions_shouldJoinThem()
    {
        $ctAnd = ConditionTree::and([
           new RawSQLString('cond1'),
           new RawSQLString('cond2'),
           new RawSQLString('cond3'),
        ]);
        self::assertEquals(
            '(cond1 AND cond2 AND cond3)',
            $ctAnd->serialize($this->escaper));

        $ctOr = ConditionTree::or([
            new RawSQLString('cond1'),
            new RawSQLString('cond2'),
            new RawSQLString('cond3'),
        ]);
        self::assertEquals(
            '(cond1 OR cond2 OR cond3)',
            $ctOr->serialize($this->escaper));
    }

    public function testCollectParameters_shouldPassthroughClauseParameters()
    {
        $canary = random_bytes(16);

        $ct = ConditionTree::and([]);
        $ct->add(WhereClause::equals('a', $canary));
        $params = $ct->collectParameters($this->escaper);
        self::assertEquals([$canary], $params);

        $ct2 = ConditionTree::and([$ct]);
        $params = $ct2->collectParameters($this->escaper);
        self::assertEquals([$canary], $params);
    }

    public function testConstructor_givenInvalidTreeType_throws()
    {
        self::expectException(\BadMethodCallException::class);
        new ConditionTree('aaaaaaAAAAAA');
    }
}
