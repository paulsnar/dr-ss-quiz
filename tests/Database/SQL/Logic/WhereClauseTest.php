<?php


namespace Quiz\Tests\Database\SQL\Logic;


use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\Logic\WhereClause;
use Quiz\Database\SQL\PassthroughEscaper;

class WhereClauseTest extends TestCase
{
    /** @var PassthroughEscaper */
    private $escaper;

    public function setUp()
    {
        $this->escaper = new PassthroughEscaper();
    }

    public function whereClauseProvider()
    {
        return [
            [ 'a', null, 'a = ?' ],
            [ 'a', '=', 'a = ?' ],
            [ 'a', '!=', 'a != ?' ],
            [ 'a', '>', 'a > ?' ],
            [ 'a', '>=', 'a >= ?' ],
            [ 'a', '<', 'a < ?' ],
            [ 'a', '<=', 'a <= ?' ],
            [ 'a', '<>', 'a <> ?' ],
            [ 'a', 'IN', 'a IN ?' ],
        ];
    }

    /**
     * @dataProvider whereClauseProvider
     */
    public function testWhereClauses($col, $oper, $result)
    {
        if ($oper !== null) {
            $clause = new WhereClause($col, 'dummy', $oper);
        } else {
            $clause = new WhereClause($col, 'dummy');
        }

        self::assertEquals($result, $clause->serialize($this->escaper));
    }

    public function whereFluentMethodProvider()
    {
        return [
            [ 'equals', '=' ],
            [ 'notEquals', '!=' ],
            [ 'lessThan', '<', false ],
            [ 'lessThan', '<=', true ],
            [ 'greaterThan', '>', false ],
            [ 'greaterThan', '>=', true ],
            [ 'in', 'IN' ],
        ];
    }

    /**
     * @dataProvider whereFluentMethodProvider
     */
    public function testWhereClauseFluentMethods(
        $method, $oper, ?bool $alsoEquals = null)
    {
        $traditional = new WhereClause('a', 'dummy', $oper);

        if ($alsoEquals !== null) {
            $fluent = WhereClause::$method('a', 'dummy', $alsoEquals);
        } else {
            $fluent = WhereClause::$method('a', 'dummy');
        }

        self::assertEquals($traditional, $fluent);
    }

    public function testWhereClauseParameterPassthrough()
    {
        $canary = random_bytes(16);
        $clause = new WhereClause('a', $canary);
        self::assertEquals($canary, $clause->getParameter($this->escaper));
    }
}
