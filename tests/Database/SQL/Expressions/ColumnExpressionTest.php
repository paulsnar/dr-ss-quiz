<?php


namespace Quiz\Tests\Database\SQL\Expressions;


use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\{EscaperInterface, SQLSerializableInterface};
use Quiz\Database\SQL\Expressions\ColumnExpression;

/**
 * @coversDefaultClass \Quiz\Database\SQL\Expressions\ColumnExpression
 * @covers \Quiz\Database\SQL\Expressions\ColumnExpression::__construct
 */
class ColumnExpressionTest extends TestCase
{
    /** @var EscaperInterface|MockObject */
    private $escaper;

    public function setUp()
    {
        $this->escaper = self::createMock(
            EscaperInterface::class);
    }

    /**
     * @covers ::__construct
     */
    public function testFluentInterface_setsConstructorArgumentAsColumn()
    {
        $column = 'some column';
        $col = new ColumnExpression($column);
        self::assertEquals($column, $col->column);
    }

    /**
     * @uses \Quiz\Database\SQL\Expressions\ColumnExpression::__construct
     * @covers ::as
     */
    public function testFluentInterface_as_setsColumnAndAlias()
    {
        $column = 'some column';
        $alias = 'some alias';
        $col = ColumnExpression::as($column, $alias);
        self::assertEquals($column, $col->column);
        self::assertEquals($alias, $col->alias);
    }

    /**
     * @depends testFluentInterface_setsConstructorArgumentAsColumn
     * @covers ::serialize
     */
    public function test_withoutAlias_returnsColumnName()
    {
        $column = 'some column';

        $this->escaper->expects(self::once())
            ->method('identifier')
            ->with($column)
            ->willReturn($column);

        $col = new ColumnExpression($column);
        self::assertEquals($column, $col->serialize($this->escaper));
    }

    /**
     * @depends testFluentInterface_as_setsColumnAndAlias
     * @covers ::serialize
     * @uses \Quiz\Database\SQL\Expressions\ColumnExpression::as
     */
    public function test_withAlias_returnsAliasedColumnName()
    {
        $column = 'some column';
        $alias = 'some alias';

        $this->escaper->expects(self::exactly(2))
            ->method('identifier')
            ->withConsecutive([$column], [$alias])
            ->willReturnArgument(0);

        $col = ColumnExpression::as($column, $alias);
        self::assertEquals("{$column} AS {$alias}",
            $col->serialize($this->escaper));
    }

    /**
     * @covers ::serialize
     */
    public function test_givenASerializableAsColumnDefinition_serializesIt()
    {
        $raw = 'some random expression with spaces in it';
        $col = self::createMock(SQLSerializableInterface::class);
        $col->expects(self::once())
            ->method('serialize')
            ->with($this->escaper)
            ->willReturn($raw);

        $expression = new ColumnExpression($col);
        self::assertEquals($raw, $expression->serialize($this->escaper));
    }
}
