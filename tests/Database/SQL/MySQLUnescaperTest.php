<?php


namespace Quiz\Tests\Database\SQL;


use PHPUnit\Framework\TestCase;
use Quiz\Database\SQL\MySQLUnescaper;

/**
 * @coversDefaultClass \Quiz\Database\SQL\MySQLUnescaper
 */
class MySQLUnescaperTest extends TestCase
{
    /** @var MySQLUnescaper */
    private $unescaper;

    public function setUp()
    {
        $this->unescaper = new MySQLUnescaper();
    }

    public function unescapeDataProvider()
    {
        return [
            [ 'bool', '0', false ],
            [ 'bool', '1', true ],
            [ 'int', '42', 42 ],
            [ 'float', '0.25', 0.25 ],
            [ '\\DateTime', '1970-01-01 00:00:00',
                new \DateTime('1970-01-01T00:00:00Z') ],
            [ '\\DateTimeInterface', '1970-01-01 00:00:00',
                new \DateTimeImmutable('1970-01-01T00:00:00Z') ],
            [ '\\DateTimeImmutable', '1970-01-01 00:00:00',
                new \DateTimeImmutable('1970-01-01T00:00:00Z') ],
            [ 'string', 'hello', 'hello' ],
        ];
    }

    /**
     * @dataProvider unescapeDataProvider
     * @covers ::unescape
     */
    public function testUnescaping($typehint, $input, $expected)
    {
        self::assertEquals($expected,
            $this->unescaper->unescape($input, $typehint));
        if ($typehint[0] === '\\') {
            self::assertEquals($expected,
                $this->unescaper->unescape($input, substr($typehint, 1)));
        }
    }
}
