<?php

namespace Quiz\Tests\Database\ORM;

use Quiz\Database\ORM\{Model, ModelCondition};
use PHPUnit\Framework\TestCase;

class _SimpleModel extends Model
{
    public $id, $serializableColumn;

    /** @hiddenJSON */
    public $hidden;

    // override for safer code coverage
    public static function getPrimaryKey(): array { return ['id']; }
}

class _CompositePrimaryKeyModel extends Model
{
    public $id1, $id2;

    public static function getPrimaryKey(): array { return ['id1', 'id2']; }
}

/**
 * @coversDefaultClass \Quiz\Database\ORM\Model
 */
class ModelTest extends TestCase
{
    /**
     * @covers ::doesExist
     * @covers ::isNew
     */
    public function test_withSimplePrimaryKey_doesExistAndIsNew_checksForKey()
    {
        $a = new _SimpleModel();
        self::assertFalse($a->doesExist());
        self::assertTrue($a->isNew());

        $a->id = 42;
        self::assertTrue($a->doesExist());
        self::assertFalse($a->isNew());
    }

    /**
     * @covers ::doesExist
     * @covers ::isNew
     */
    public function test_withCompositePrimaryKey_doesExistAndIsNew_checksForAllKeys()
    {
        $b = new _CompositePrimaryKeyModel();
        self::assertFalse($b->doesExist());
        self::assertTrue($b->isNew());

        $b->id1 = 42;
        self::assertFalse($b->doesExist());
        self::assertTrue($b->isNew());

        $b->id2 = 420;
        self::assertTrue($b->doesExist());
        self::assertFalse($b->isNew());
    }

    /**
     * @covers ::getPrimaryKey
     */
    public function testDefaultPrimaryKey()
    {
        self::assertEquals(['id'], Model::getPrimaryKey());
    }

    /**
     * @covers ::jsonSerialize
     * @uses \Quiz\Database\ORM\ModelMetadata
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function test_givenModelInstance_jsonSerialize_serializesPropsFromMetadata()
    {
        // Note that since jsonSerialize basically iterates over metadata
        // properties, this test is rather loose. Please look into
        // ModelMetadata tests instead, there is a lot of black magic involved.
        $a = new _SimpleModel();
        $a->id = 42;
        $a->serializableColumn = 'hello';
        $a->hidden = 'secret!';
        self::assertEquals(
            ['id' => 42, 'serializable_column' => 'hello'],
            $a->jsonSerialize());
    }

    /**
     * @covers ::query
     * @uses \Quiz\Database\ORM\ModelCondition
     */
    public function test_query_returnsModelCondition()
    {
        $cond = [ 'id' => 42 ];
        $query = _SimpleModel::query($cond);
        self::assertInstanceOf(ModelCondition::class, $query);
        self::assertEquals(_SimpleModel::class, $query->class);
        self::assertEquals($cond, $query->conditions);
    }

    /**
     * @covers ::__set
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     */
    public function test_overloadedSetter_setsStubReferences()
    {
        $a = new _SimpleModel();
        $a->someId = 42;
        self::assertEquals(['some' => 42], $a->_stubReferences);
    }

    /**
     * @covers ::__set
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     */
    public function test_overloadedSetter_throwsOnUnknownProperties()
    {
        $a = new _SimpleModel();
        self::expectException(\Exception::class);
        $a->unknownProperty = 42;
    }
}
