<?php

namespace Quiz\Tests\Database\ORM;

use Quiz\Database\ORM\{IncompleteObjectException, Model, ModelMetadata, ModelProperty};
use Quiz\Database\SQL\UnescaperInterface;
use PHPUnit\Framework\{MockObject, TestCase};

// Classes for testing table inflection.

class _ModelExplicitTable
{
    const TABLE = 'model_table';
}

class _ModelStaticTableGetMethod
{
    public static function getTable()
    {
        return 'model_table';
    }
}

class _ModelNonStaticTableGetMethod { public function getTable() { } }

class TableModel { }

// Classes for testing property detection.

class _ModelSimplePublicProperties
{
    const TABLE = '';
    public $a;

    /** @var stdClass */
    public $b;
}

class _ModelUnderscorePrefixedScalarPublicProperties
{
    const TABLE = '';
    public $_a;
}

class _ModelNonPublicProperties
{
    const TABLE = '';

    protected $a;
    private $b;

    /** @var stdClass */
    protected $c;

    /** @var stdClass */
    private $d;
}

// Classes for loadFromRow testing.
class _ModelScalarsOnly
{
    const TABLE = '';
    public $a;
}

class ReferencedModel extends Model
{
}

class _ModelHasReference extends Model
{
    const TABLE = '';

    /** @var string */
    public $a;

    /** @var \Quiz\Tests\Database\ORM\ReferencedModel */
    public $b;
}

// Classes for general testing.

class _Model { }

/**
 * @coversDefaultClass \Quiz\Database\ORM\ModelMetadata
 * @uses \Quiz\Database\ORM\ModelMetadata::__construct
 */
class ModelMetadataTest extends TestCase
{
    /**
     * @covers ::fromModelName
     * @uses \Quiz\Database\ORM\ModelMetadata::parseTableNameFromModel
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function test_fromModelName_infersTableName()
    {
        $meta = ModelMetadata::fromModelName(_ModelExplicitTable::class);
        self::assertEquals(_ModelExplicitTable::TABLE, $meta->table);

        $meta = ModelMetadata::fromModelName(_ModelStaticTableGetMethod::class);
        self::assertEquals(_ModelStaticTableGetMethod::getTable(), $meta->table);

        $meta = ModelMetadata::fromModelName(TableModel::class);
        self::assertEquals('tables', $meta->table);
    }

    /**
     * @covers ::fromModelName
     */
    public function test_fromModelName_throwsIfGetTableMethodIsDefinedButNotStatic()
    {
        self::expectException(\Exception::class);
        ModelMetadata::fromModelName(_ModelNonStaticTableGetMethod::class);
    }

    /**
     * @covers ::fromModelName
     * @uses \Quiz\Database\ORM\ModelMetadata::parseTableNameFromModel
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function test_fromModelName_detectsPublicProperties()
    {
        $meta = ModelMetadata::fromModelName(
            _ModelSimplePublicProperties::class);

        self::assertCount(2, $meta->properties);
        self::assertEquals([ 'a', 'b' ], array_keys($meta->properties));
    }

    /**
     * @covers ::fromModelName
     * @uses \Quiz\Database\ORM\ModelMetadata::parseTableNameFromModel
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function test_fromModelName_skipsUnderscorePrefixedPublicScalarProperties()
    {
        $meta = ModelMetadata::fromModelName(
            _ModelUnderscorePrefixedScalarPublicProperties::class);

        self::assertCount(0, $meta->properties);
    }

    /**
     * @covers ::fromModelName
     * @uses \Quiz\Database\ORM\ModelMetadata::parseTableNameFromModel
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function test_fromModelName_skipsNonPublicProperties()
    {
        $meta = ModelMetadata::fromModelName(
            _ModelNonPublicProperties::class);

        self::assertCount(0, $meta->properties);
    }

    /**
     * @covers ::__construct
     */
    public function testConstructorSetsClassName()
    {
        $meta = new ModelMetadata(_Model::class);
        self::assertEquals(_Model::class, $meta->className);
    }

    public function modelTableNameProvider()
    {
        return [
            [ 'ObjectModel', 'objects' ],
            [ 'NS\\NamespaceModel', 'namespaces' ],
            [ 'MultiWordModel', 'multi_words' ],
            [ 'QuizModel', 'quizzes' ], // re: pluralize
        ];
    }

    /**
     * @covers ::parseTableNameFromModel
     * @dataProvider modelTableNameProvider
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     */
    public function testParseTableNameFromModel($model, $expected)
    {
        self::assertEquals(
            $expected, ModelMetadata::parseTableNameFromModel($model));
    }

    /**
     * @covers ::parseTableNameFromModel
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     */
    public function test_parseTableNameFromModel_throwsOnContractViolation()
    {
        self::expectException(\Exception::class);
        ModelMetadata::parseTableNameFromModel('NonCompliantClass');
    }

    /**
     * @covers ::instantiate
     */
    public function testInstantiate()
    {
        $meta = new ModelMetadata(_Model::class);
        self::assertInstanceOf(_Model::class, $meta->instantiate());
    }

    /**
     * @covers ::getProperty
     * @uses \Quiz\Database\ORM\ModelMetadata::fromModelName
     * @uses \Quiz\Database\ORM\ModelMetadata::parseTableNameFromModel
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\snake_case_to_camel_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function testGetProperty()
    {
        $meta = ModelMetadata::fromModelName(
            _ModelSimplePublicProperties::class);

        self::assertSame($meta->properties['a'], $meta->getProperty('a'));
        self::assertSame($meta->properties['b'], $meta->getProperty('b_id'));
    }

    /**
     * @covers ::loadFromRow
     * @uses \Quiz\Database\ORM\IncompleteObjectException
     * @uses \Quiz\Database\ORM\ModelMetadata::fromModelName
     * @uses \Quiz\Database\ORM\ModelMetadata::instantiate
     * @uses \Quiz\Database\ORM\ModelMetadata::parseTableNameFromModel
     * @uses \Quiz\Database\ORM\ModelProperty
     * @uses \Quiz\Database\ORM\ModelReference
     * @uses ::\Quiz\FunctionalUtilities\camel_case_to_snake_case
     * @uses ::\Quiz\FunctionalUtilities\pluralize
     * @uses ::\Quiz\FunctionalUtilities\str_ends_with
     * @uses ::\Quiz\FunctionalUtilities\str_starts_with
     */
    public function testLoadFromRow()
    {
        /** @var UnescaperInterface|MockObject $unescaper */
        $unescaper = self::createMock(UnescaperInterface::class);
        $unescaper->expects(self::once())
            ->method('unescape')
            ->with('value', 'string')
            ->willReturn('value');

        $meta = ModelMetadata::fromModelName(_ModelScalarsOnly::class);
        $inst = $meta->loadFromRow($unescaper, [ 'a' => 'value' ]);

        self::assertInstanceOf(_ModelScalarsOnly::class, $inst);
        self::assertEquals('value', $inst->a);

        /** @var UnescaperInterface|MockObject $unescaper */
        $unescaper = self::createMock(UnescaperInterface::class);
        $unescaper->expects(self::once())
            ->method('unescape')
            ->with('value', 'string')
            ->willReturn('value');

        $meta = ModelMetadata::fromModelName(_ModelHasReference::class);
        try {
            $inst = $meta->loadFromRow($unescaper,
                [ 'a' => 'value', 'b_id' => 42 ]);
            self::fail("loadFromRow didn't throw IncompleteObjectException " .
                'for incomplete object');
        } catch (IncompleteObjectException $err) {
            $exception = $err;
        }
        $inst = $exception->instance;

        self::assertSame($exception->model, $meta);
        self::assertInstanceOf(_ModelHasReference::class, $inst);

        $ref = $exception->references[0];
        self::assertEquals('b', $ref->name);
    }
}
