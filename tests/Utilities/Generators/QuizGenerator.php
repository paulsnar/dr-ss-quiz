<?php


namespace Quiz\Tests\Utilities\Generators;


use Quiz\Models\QuizModel;

trait QuizGenerator
{
    protected function generateQuiz(): QuizModel
    {
        static $i = 0;
        $quiz = new QuizModel();
        $quiz->id = ++$i;
        $quiz->name = "Quiz {$i}";
        return $quiz;
    }
}
