<?php


namespace Quiz\Tests\Utilities\Generators;


use Quiz\Models\UserModel;

trait UserGenerator
{
    protected function generateUser(): UserModel
    {
        static $i = 0;
        $user = new UserModel();
        $user->id = ++$i;
        $user->name = "User {$i}";
        return $user;
    }
}
