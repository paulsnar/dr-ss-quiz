<?php


namespace Quiz\Tests\Utilities\Generators;


use Quiz\Models\{AnswerModel, UserAnswerModel, UserModel};

trait UserAnswerGenerator
{
    protected function generateUserAnswer(
        UserModel $user,
        AnswerModel $answer
    ): UserAnswerModel {
        static $i = 0;
        $userAnswer = new UserAnswerModel();
        $userAnswer->id = ++$i;
        $userAnswer->user = $user;
        $userAnswer->quiz = $answer->question->quiz;
        $userAnswer->question = $answer->question;
        $userAnswer->answer = $answer;
        return $userAnswer;
    }
}
