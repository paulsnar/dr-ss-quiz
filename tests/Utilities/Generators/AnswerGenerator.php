<?php


namespace Quiz\Tests\Utilities\Generators;


use Quiz\Models\{AnswerModel, QuestionModel};

trait AnswerGenerator
{
    protected function generateAnswer(QuestionModel $question): AnswerModel
    {
        static $i = 0;
        $answer = new AnswerModel();
        $answer->id = ++$i;
        $answer->question = $question;
        $answer->answer = "Answer {$i}";
        $answer->isCorrect = rand(0, 10) > 6;
        return $answer;
    }
}
