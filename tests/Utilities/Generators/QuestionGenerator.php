<?php


namespace Quiz\Tests\Utilities\Generators;


use Quiz\Models\{QuizModel, QuestionModel};

trait QuestionGenerator
{
    protected function generateQuestion(QuizModel $quiz): QuestionModel
    {
        static $i = 0;
        $question = new QuestionModel();
        $question->id = ++$i;
        $question->quiz = $quiz;
        $question->question = "Question {$i}";
        return $question;
    }
}
