<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Quiz\Tests\Utilities\InterfaceCompliance\Repositories;


use PHPUnit\Framework\TestCase;
use Quiz\Exceptions\ItemNotFoundException;
use function Quiz\FunctionalUtilities\array_pick;
use Quiz\Models\{AnswerModel, QuestionModel, QuizModel};
use Quiz\Repositories\QuizRepositoryInterface;
use Quiz\Tests\Utilities\QuizRepositoryMockDataImporter;

abstract class QuizRepositoryInterfaceTest extends TestCase
{
    /** @var QuizRepositoryInterface */
    private $quizRepo;

    /** @var QuizModel[] */
    private $quizzes;

    /** @var QuestionModel[] */
    private $questions;

    abstract protected function getImplementation(): QuizRepositoryInterface;

    public function setUp()
    {
        $dump = QuizRepositoryMockDataImporter::getData();

        $this->quizRepo = $this->getImplementation();
        $this->quizzes = QuizRepositoryMockDataImporter::installQuizzesFromData(
            $dump, $this->quizRepo);
        $this->questions =
            QuizRepositoryMockDataImporter::installQuestionsFromData(
                $dump, $this->quizzes, $this->quizRepo);

        QuizRepositoryMockDataImporter::installAnswersFromData(
            $dump, $this->questions, $this->quizRepo);

        parent::setUp();
    }

    private function questionsByQuiz()
    {
        $grouped = [];
        foreach ($this->questions as $question) {
            if (array_key_exists($question->quizId, $grouped)) {
                $grouped[$question->quizId][] = $question;
            } else {
                $grouped[$question->quizId] = [$question];
            }
        }
        return $grouped;
    }

    public function testQuizRetrievalById()
    {
        foreach ($this->quizzes as $knownQuiz) {
            $quiz = $this->quizRepo->get($knownQuiz->id);

            self::assertNotNull($quiz);
            self::assertEquals($knownQuiz, $quiz);
        }
    }

    public function testQuizRetrievalByIdThrowsWhenNonExistent()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->quizRepo->get(0);
    }

    public function testQuizListRetrieval()
    {
        $quizzes = $this->quizRepo->getAll();
        self::assertNotEmpty($quizzes);
        self::assertEquals($this->quizzes, $quizzes);
    }

    /**
     * @depends testQuizRetrievalById
     */
    public function testQuestionRetrievalForQuizId()
    {
        $allKnownQuestions = $this->questionsByQuiz();

        foreach ($this->quizzes as $quiz) {
            $knownQuestions = $allKnownQuestions[$quiz->id] ?? [];
            $questions = $this->quizRepo->getQuestionsForQuiz($quiz->id);
            self::assertEquals($knownQuestions, $questions);
        }

        self::assertEquals([], $this->quizRepo->getQuestionsForQuiz(-1));
    }

    public function testGetQuestionCount()
    {
        $questionsByQuiz = [];
        foreach ($this->questions as $question) {
            if (array_key_exists($question->quizId, $questionsByQuiz)) {
                $questionsByQuiz[$question->quizId][] = $question;
            } else {
                $questionsByQuiz[$question->quizId] = [$question];
            }
        }

        foreach ($this->quizzes as $quiz) {
            $questions = $questionsByQuiz[$quiz->id];
            self::assertEquals(
                count($questions),
                $this->quizRepo->getQuestionCount($quiz->id)
            );
        }
    }

    /**
     * @depends testQuestionRetrievalForQuizId
     */
    public function testAnswerRetrievalForQuestionId()
    {
        foreach ($this->quizzes as $quiz) {
            $questions = $this->quizRepo->getQuestionsForQuiz($quiz->id);
            $question = $questions[0];

            $answers = $this->quizRepo->getAnswersForQuestion($question->id);
            self::assertNotEmpty($answers);
        }
    }

    /**
     * @depends testQuestionRetrievalForQuizId
     * @depends testAnswerRetrievalForQuestionId
     */
    public function testAnswerRetrievalById()
    {
        /** @var AnswerModel[] $answers */
        $answers = [];

        foreach ($this->quizzes as $quiz) {
            foreach ($this->quizRepo->getQuestionsForQuiz($quiz->id) as $question) {
                $moreAnswers = $this->quizRepo->getAnswersForQuestion($question->id);
                $answers = array_merge($answers, $moreAnswers);
            }
        }

        foreach ($answers as $knownAnswer) {
            $answer = $this->quizRepo->getAnswer($knownAnswer->id);
            self::assertEquals($knownAnswer, $answer);
        }
    }

    public function testAnswerRetrievalByIdThrowsWhenNonExistent()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->quizRepo->getAnswer(-1);
    }

    /**
     * @depends testQuestionRetrievalForQuizId
     */
    public function testQuestionRetrievalById()
    {
        foreach ($this->quizzes as $quiz) {
            $questions = $this->quizRepo->getQuestionsForQuiz($quiz->id);
            foreach ($questions as $knownQuestion) {
                $question = $this->quizRepo->getQuestion($knownQuestion->id);
                self::assertEquals($knownQuestion, $question);
            }
        }
    }

    public function testQuestionRetrievalByIdThrowsWhenNonExistent()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->quizRepo->getQuestion(-1);
    }
}
