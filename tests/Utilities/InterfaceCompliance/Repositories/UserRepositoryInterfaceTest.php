<?php /** @noinspection PhpUnhandledExceptionInspection */


namespace Quiz\Tests\Utilities\InterfaceCompliance\Repositories;


use PHPUnit\Framework\TestCase;
use Quiz\Exceptions\ItemNotFoundException;
use Quiz\Models\UserModel;
use Quiz\Repositories\UserRepositoryInterface;

abstract class UserRepositoryInterfaceTest extends TestCase
{
    /** @var UserRepositoryInterface */
    private $userRepo;

    abstract protected function getImplementation(): UserRepositoryInterface;

    public function setUp()
    {
        $this->userRepo = $this->getImplementation();
        parent::setUp();
    }

    public function testUserCreation()
    {
        $user = new UserModel('User');
        self::assertTrue($user->isNew());

        $user = $this->userRepo->create($user);
        self::assertFalse($user->isNew());

        return $user;
    }

    public function testUserUpdating()
    {
        $user = $this->userRepo->create(new UserModel('user'));

        $user2 = clone $user;
        $user = $this->userRepo->update($user);
        self::assertEquals($user2, $user);
    }

    public function testUserExistence()
    {
        self::assertFalse($this->userRepo->exists(-1));

        $user = $this->userRepo->create(new UserModel('user'));
        self::assertTrue($this->userRepo->exists($user->id));
    }

    /**
     * @depends testUserExistence
     */
    public function testUserDeletion()
    {
        $user = $this->userRepo->create(new UserModel('user'));
        self::assertTrue($user->doesExist());

        $prevId = $user->id;
        $this->userRepo->delete($user);

        self::assertFalse($user->doesExist());
        self::assertFalse($this->userRepo->exists($prevId));
    }

    public function testUserLookupById()
    {
        $knownUser = $this->testUserCreation();

        $user = $this->userRepo->get($knownUser->id);
        self::assertEquals($knownUser, $user);
    }

    public function testUserLookupByIdThrowsWhenNonExistent()
    {
        $this->expectException(ItemNotFoundException::class);
        $this->userRepo->get(-1);
    }
}
