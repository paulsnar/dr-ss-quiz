<?php


namespace Quiz\Tests\Utilities\InterfaceCompliance\Repositories;


use PHPUnit\Framework\TestCase;
use Quiz\Models\{QuizModel, UserAnswerModel, UserModel};
use Quiz\Repositories\{QuizRepositoryInterface, UserAnswerRepositoryInterface, UserRepositoryInterface};
use Quiz\Tests\Utilities\QuizRepositoryMockDataImporter;
use function Quiz\FunctionalUtilities\array_pick;

abstract class UserAnswerRepositoryInterfaceTest extends TestCase
{
    /** @var QuizRepositoryInterface */
    private $quizRepo;

    /** @var QuizModel[] */
    private $quizzes;

    /** @var UserRepositoryInterface */
    private $userRepo;

    /** @var UserModel */
    private $user;

    /** @var UserAnswerRepositoryInterface */
    private $userAnswerRepo;

    public function setUp()
    {
        $this->quizRepo = $this->getQuizRepositoryImplementation();
        $this->quizzes = QuizRepositoryMockDataImporter::importData(
            $this->quizRepo
        );

        $this->userRepo = $this->getUserRepositoryImplementation();
        $this->user = $this->userRepo->create(new UserModel('Test User'));

        $this->userAnswerRepo = $this->getImplementation();
    }

    abstract protected function getQuizRepositoryImplementation():
    QuizRepositoryInterface;

    abstract protected function getUserRepositoryImplementation():
    UserRepositoryInterface;

    abstract protected function getImplementation():
    UserAnswerRepositoryInterface;

    public function testAnswerSaving()
    {
        foreach ($this->quizzes as $quiz) {
            /** @var UserAnswerModel[] $knownUserAnswers */
            $knownUserAnswers = [];

            foreach (
                $this->quizRepo->getQuestionsForQuiz($quiz->id) as $question
            ) {
                $answers = $this->quizRepo->getAnswersForQuestion(
                    $question->id);
                $answer = array_pick($answers);

                $knownUserAnswers[] = $this->userAnswerRepo->create(
                    new UserAnswerModel($this->user->id, $quiz->id, $answer->id)
                );
            }

            $userAnswers = $this->userAnswerRepo->getAllForSession(
                $this->user->id, $quiz->id);

            self::assertEquals($knownUserAnswers, $userAnswers);
        }
    }

    /**
     * @depends testAnswerSaving
     */
    public function testCountForSession()
    {
        foreach ($this->quizzes as $quiz) {
            $num = 0;
            foreach (
                $this->quizRepo->getQuestionsForQuiz($quiz->id) as $question
            ) {
                self::assertEquals(
                    $num,
                    $this->userAnswerRepo->getCountForSession(
                        $this->user->id, $quiz->id)
                );

                $answers = $this->quizRepo->getAnswersForQuestion(
                    $question->id);
                $answer = array_pick($answers);
                $this->userAnswerRepo->create(
                    new UserAnswerModel($this->user->id, $quiz->id, $answer->id)
                );

                $num += 1;
                self::assertEquals(
                    $num,
                    $this->userAnswerRepo->getCountForSession(
                        $this->user->id, $quiz->id)
                );
            }

            self::assertEquals(
                $num,
                $this->userAnswerRepo->getCountForSession(
                    $this->user->id, $quiz->id)
            );
        }
    }
}
