<?php

namespace Quiz\UtilityTests\Structs\BagTest;

use Quiz\Utilities\Structs\Bag;
use PHPUnit\Framework\TestCase;

/**
 * @coversDefaultClass \Quiz\Utilities\Structs\Bag
 * @uses \Quiz\Utilities\Structs\Bag::__construct
 */
class BagTest extends TestCase
{
    /** @var Bag */
    private $empty, $full;

    public function setUp()
    {
        $this->empty = new Bag();
        $this->full = new Bag(['anything' => 42]);
    }

    /**
     * @covers ::__construct
     */
    public function testConstructor()
    {
        self::assertTrue(true); // did not throw on setUp
    }

    /**
     * @covers ::get
     * @depends testConstructor
     */
    public function testGet()
    {
        self::assertNull($this->empty->get('anything'));
        self::assertEquals(42, $this->full->get('anything'));
    }

    /**
     * @covers ::contains
     * @depends testConstructor
     */
    public function testContains()
    {
        self::assertFalse($this->empty->contains('anything'));
        self::assertTrue($this->full->contains('anything'));
    }

    /**
     * @covers ::set
     * @depends testGet
     * @uses \Quiz\Utilities\Structs\Bag::get
     */
    public function testSet()
    {
        $this->empty->set('anything', 42);
        self::assertEquals(42, $this->empty->get('anything'));

        $this->full->set('anything', 420);
        self::assertEquals(420, $this->full->get('anything'));
    }

    /**
     * @covers ::unset
     * @depends testContains
     * @uses \Quiz\Utilities\Structs\Bag::contains
     */
    public function testUnset()
    {
        self::assertFalse($this->empty->contains('anything'));
        $this->empty->unset('anything');
        self::assertFalse($this->empty->contains('anything'));

        self::assertTrue($this->full->contains('anything'));
        $this->full->unset('anything');
        self::assertFalse($this->full->contains('anything'));
    }

    /**
     * @covers ::all
     */
    public function testAll()
    {
        foreach ($this->empty->all() as $key => $value) {
            self::fail('empty bag iterator produced values');
        }

        foreach ($this->full->all() as $key => $value) {
            self::assertEquals('anything', $key);
            self::assertEquals(42, $value);
        }
    }
}
