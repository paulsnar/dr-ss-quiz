<?php


namespace Quiz\Tests;

use PHPUnit\Framework\TestCase;
use function Quiz\FunctionalUtilities\{array_pick, vector_filter,
    snake_case_to_camel_case, camel_case_to_snake_case, times};

class FunctionalUtilityTest extends TestCase
{
    /**
     * @covers ::Quiz\FunctionalUtilities\vector_filter
     */
    public function testVectorFilter()
    {
        $sample = [1, 2, 3, 4, 5];

        $empty = vector_filter($sample, function () {
            return false;
        });
        self::assertCount(0, $empty);

        $all = vector_filter($sample, function () {
            return true;
        });
        self::assertEquals($sample, $all);

        $some = vector_filter($sample, function ($item) {
            return $item % 2 === 0;
        });
        self::assertCount(2, $some);

        // Checks that the keys are *not* preserved, i.e., sequence semantics
        // are retained.
        self::assertNotEquals(
            array_search(4, $some),
            array_search(4, $sample)
        );
    }

    /**
     * @covers ::Quiz\FunctionalUtilities\array_pick
     */
    public function testArrayPick()
    {
        try {
            array_pick([]);
            self::fail(
                'Did not raise an exception when picking from an empty array');
        } catch (\Throwable $e) {
            // noop
            self::assertTrue(true);
        }

        $a = [42];
        self::assertEquals($a[0], array_pick($a));

        $b = [1, 2, 3, 4, 5];
        self::assertTrue(in_array(array_pick($b), $b));
    }

    /**
     * @covers ::Quiz\FunctionalUtilities\snake_case_to_camel_case
     */
    public function testSnakeCaseToCamelCase()
    {
        self::assertEquals('',
            snake_case_to_camel_case(''));
        self::assertEquals('plainstring',
            snake_case_to_camel_case('plainstring'));
        self::assertEquals('camelCasedString',
            snake_case_to_camel_case('camel_cased_string'));
    }

    /**
     * @covers ::Quiz\FunctionalUtilities\camel_case_to_snake_case
     */
    public function testCamelCaseToSnakeCase()
    {
        self::assertEquals('',
            camel_case_to_snake_case(''));
        self::assertEquals('plainstring',
            camel_case_to_snake_case('plainstring'));
        self::assertEquals('snake_cased_string',
            camel_case_to_snake_case('snakeCasedString'));
    }

    /**
     * @covers ::Quiz\FunctionalUtilities\times
     */
    public function testTimes()
    {
        $n = 0;
        $increment = function () use (&$n) {
            $n += 1;
        };

        times(0, $increment);
        self::assertEquals(0, $n);

        times(1, $increment);
        self::assertEquals(1, $n);

        $times = rand(0, 100);
        $n = 0;
        times($times, $increment);
        self::assertEquals($times, $n);

        $prevI = -1;
        times($times, function ($i) use (&$prevI) {
            if ($i < $prevI) {
                self::fail('Argument to callback was not incremented');
            }
            $prevI = $i;
        });
    }
}
