<?php

define('QUIZ_ROOT', dirname(__DIR__));
define('QUIZ_PUBLIC_ROOT', __DIR__);

require QUIZ_ROOT . '/vendor/autoload.php';

(new \Quiz\QuizApplication())->bootstrap()->dispatchRequest();
