<?php


namespace Quiz\Models;


use Quiz\Database\ORM\Model;

class UserResultModel extends Model
{
    /** @var int */
    public $id;

    /** @var \Quiz\Models\QuizModel */
    public $quiz;

    /** @var \Quiz\Models\UserModel */
    public $user;

    /** @var int */
    public $score;

    /** @var \DateTimeInterface */
    public $createdAt;
}
