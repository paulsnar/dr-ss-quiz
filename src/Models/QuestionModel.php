<?php


namespace Quiz\Models;


use Quiz\Database\ORM\Model;

class QuestionModel extends Model
{
    /** @var int */
    public $id;

    /** @var string */
    public $question;

    /** @var \Quiz\Models\QuizModel */
    public $quiz;

    /** @var int */
    public $orderWithinQuiz;
}
