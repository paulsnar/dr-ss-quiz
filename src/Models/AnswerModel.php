<?php


namespace Quiz\Models;


use Quiz\Database\ORM\Model;

class AnswerModel extends Model
{
    /** @var int */
    public $id;

    /** @var string */
    public $answer;

    /** @var \Quiz\Models\QuestionModel */
    public $question;

    /**
     * @var bool
     * @hiddenJSON
     */
    public $isCorrect;
}
