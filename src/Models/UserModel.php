<?php


namespace Quiz\Models;


use Quiz\Database\ORM\Model;

class UserModel extends Model
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var \DateTimeInterface */
    public $createdAt;
}
