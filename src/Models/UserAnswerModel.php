<?php


namespace Quiz\Models;


use Quiz\Database\ORM\Model;

class UserAnswerModel extends Model
{
    /** @var int */
    public $id;

    /** @var \Quiz\Models\UserModel */
    public $user;

    /** @var \Quiz\Models\QuizModel */
    public $quiz;

    /** @var \Quiz\Models\QuestionModel */
    public $question;

    /** @var \Quiz\Models\AnswerModel */
    public $answer;

    /** @var \DateTimeInterface */
    public $createdAt;
}
