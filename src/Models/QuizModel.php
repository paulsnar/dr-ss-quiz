<?php


namespace Quiz\Models;


use Quiz\Database\ORM\Model;

class QuizModel extends Model
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;
}
