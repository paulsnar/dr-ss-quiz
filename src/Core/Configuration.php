<?php


namespace Quiz\Core;


class Configuration
{
    /** @var array */
    private $data;

    public function loadFrom(DependencyContainer $dc, string $path)
    {
        /** @noinspection PhpIncludeInspection */
        $this->data = require $path;

        if (array_key_exists('providers', $this->data)) {
            foreach ($this->data['providers'] as $interface => $class) {
                $dc->provides($interface, $class);
            }
        }
    }

    public function get(string $key)
    {
        return $this->data[$key] ?? null;
    }
}
