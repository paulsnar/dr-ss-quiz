<?php


namespace Quiz\Core;


class DependencyContainer
{
    private $implementations = [];

    private $singletons = [];

    private $constructors = [];

    public function get(string $classOrInterface)
    {
        if ($classOrInterface === static::class) {
            return $this;
        }

        if (array_key_exists($classOrInterface, $this->implementations)) {
            $class = $this->implementations[$classOrInterface];
        } else {
            $class = $classOrInterface;
        }

        if (array_key_exists($class, $this->constructors)) {
            return $this->constructors[$class]($this);
        }

        if (array_key_exists($class, $this->singletons)) {
            return $this->singletons[$class];
        }

        $instance = $this->instantiate($class);
        $this->singletons[$class] = $instance;
        return $instance;
    }

    protected function instantiate(string $className)
    {
        $class = new \ReflectionClass($className);

        if ($class->isAbstract() || $class->isInterface()) {
            throw new \Exception("Cannot instantiate abstract class " .
                "or interface {$className}");
        }

        $constructor = $class->getConstructor();
        if ($constructor === null) {
            // class doesn't have a constructor in its inheritance chain
            return new $className();
        }

        $arguments = [];
        $parameters = $constructor->getParameters();
        foreach ($parameters as $parameter) {
            $type = $parameter->getType();
            if ($type === null) {
                // warn?
                $arguments[] = null;
            } else {
                try {
                    $arguments[] = $this->get($type->getName());
                } catch (\Throwable $err) {
                    if ($parameter->allowsNull()) {
                        $arguments[] = null;
                    } else {
                        throw $err;
                    }
                }
            }
        }

        return new $className(...$arguments);
    }

    public function provides(string $interface, $classOrConstructor)
    {
        if (is_callable($classOrConstructor)) {
            $this->constructors[$interface] = $classOrConstructor;
        } else if (is_string($classOrConstructor)) {
            $this->implementations[$interface] = $classOrConstructor;
        } else {
            $this->singletons[$interface] = $classOrConstructor;
        }
    }

    public function store($instance)
    {
        $type = get_class($instance);
        $this->singletons[$type] = $instance;
    }

    public function call($classOrInstance, string $methodName, ...$extraArgs)
    {
        if (is_string($classOrInstance)) {
            $type = $classOrInstance;
            $instance = $this->get($classOrInstance);
        } else {
            $type = get_class($classOrInstance);
            $instance = $classOrInstance;
        }

        $class = new \ReflectionClass($type);
        $method = $class->getMethod($methodName);

        if (is_object($extraArgs[0] ?? null)) {
            $firstArgType = get_class($extraArgs[0]);
        } else {
            $firstArgType = gettype($extraArgs[0]);
        }

        $arguments = [];
        foreach ($method->getParameters() as $parameter) {
            if (!$parameter->hasType()) {
                break;
            }
            $type = $parameter->getType()->getName();
            if ($type === $firstArgType) {
                break;
            }
            try {
                $arguments[] = $this->get($type);
            } catch (\Throwable $err) {
                if ($parameter->allowsNull()) {
                    $arguments[] = null;
                } else {
                    throw $err;
                }
            }
        }
        $arguments = array_merge($arguments, $extraArgs);

        return $method->invoke($instance, ...$arguments);
    }
}
