<?php

namespace Quiz\HTTP;

use Quiz\Utilities\Structs\ImmutableBag;

class Request
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_HEAD = 'HEAD';
    const METHOD_PUT = 'PUT';
    const METHOD_PATCH = 'PATCH';
    const METHOD_DELETE = 'DELETE';
    const METHOD_OPTIONS = 'OPTIONS';

    /** @var string */
    public $method;

    /** @var string */
    public $path;

    /** @var string[] */
    public $pathParts;

    /** @var HeaderBag */
    public $headers;

    /** @var ImmutableBag */
    public $query, $form, $files, $cookies;

    /** @var BasicAuthorization|string */
    public $authorization;

    public static function fromGlobals(): self
    {
        $rq = new static();

        $rq->method = $_SERVER['REQUEST_METHOD'];
        $rq->path = $_SERVER['REQUEST_URI'];

        if (strpos($rq->path, '?') !== false) {
            $queryString = substr($rq->path,
                strpos($rq->path, '?') + 1);
            parse_str($queryString, $query);
            $rq->path = substr($rq->path,
                0, strpos($rq->path, '?'));
        } else {
            $query = $_GET;
        }

        $rq->pathParts = explode('/', $rq->path);
        array_shift($rq->pathParts);
        $rq->headers = ImmutableHeaderBag::fromGlobals();
        $rq->query = new ImmutableBag($query);
        $rq->form = new ImmutableBag($_POST);
        $rq->files = new ImmutableBag($_FILES);
        $rq->cookies = new ImmutableBag($_COOKIE);

        if ($rq->headers->contains('Authorization')) {
            $rq->authorization = static::parseAuthorization(
                $rq->headers->get('Authorization'));
        }

        return $rq;
    }

    protected static function parseAuthorization(string $hd)
    {
        [ $type, $payload ] = explode(' ', $hd, 2);
        $type = strtolower($type);
        if ($type !== 'basic') {
            return "{$type} {$payload}";
        }

        return new BasicAuthorization($payload);
    }
}
