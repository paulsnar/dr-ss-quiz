<?php


namespace Quiz\HTTP;


class ImmutableHeaderBag extends HeaderBag
{
    public function set(string $key, $value)
    {
        throw new \LogicException("Tried to modify immutable Bag");
    }

    public function unset(string $key)
    {
        throw new \LogicException("Tried to modify immutable Bag");
    }
}
