<?php

namespace Quiz\HTTP;

interface HTTPSerializable
{
    public function serializeHTTP(Request $rq): Response;
}
