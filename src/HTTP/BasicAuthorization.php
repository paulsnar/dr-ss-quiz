<?php

namespace Quiz\HTTP;

class BasicAuthorization
{
    /** @var string */
    public $username, $password;

    public function __construct(string $base64EncodedPayload)
    {
        $payload = base64_decode($base64EncodedPayload);
        [ $this->username, $this->password ] = explode(':', $payload, 2);
    }
}
