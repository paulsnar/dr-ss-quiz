<?php


namespace Quiz\HTTP;

use Quiz\Utilities\Structs\Bag;
use function Quiz\FunctionalUtilities\str_starts_with;

class HeaderBag extends Bag
{
    public function __construct(array $values = [])
    {
        $headerValues = [];
        foreach ($values as $key => $value) {
            $headerValues[strtolower($key)] = $value;
        }
        parent::__construct($headerValues);
    }

    public function contains(string $key)
    {
        return parent::contains(strtolower($key));
    }

    public function get(string $key)
    {
        return parent::get(strtolower($key));
    }

    public function set(string $key, $value)
    {
        parent::set(strtolower($key), $value);
    }

    public function unset(string $key)
    {
        parent::unset(strtolower($key));
    }

    public static function fromGlobals()
    {
        $headers = [];
        foreach ($_SERVER as $key => $value) {
            if (str_starts_with('HTTP_', $key)) {
                $key = substr($key, 5);
                $key = str_replace('_', '-', $key);
                $headers[$key] = $value;
            }
        }
        return new static($headers);
    }
}
