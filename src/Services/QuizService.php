<?php


namespace Quiz\Services;


use Quiz\Exceptions\AccessViolationException;
use Quiz\Exceptions\ItemNotFoundException;
use Quiz\Models\{AnswerModel, QuestionModel, QuizModel, UserAnswerModel, UserResultModel};
use Quiz\Repositories\ObjectRepositories\GenericInterfaces\{AnswerRepositoryInterface,
    QuizRepositoryInterface,
    QuestionRepositoryInterface,
    UserAnswerRepositoryInterface};
use Quiz\Repositories\ObjectRepositoryInterface;

class QuizService
{
    /** @var ObjectRepositoryInterface */
    private $repo;

    /** @var UserService */
    private $users;

    public function __construct(
        ObjectRepositoryInterface $repo,
        UserService $users
    ) {
        $this->repo = $repo;
        $this->users = $users;
    }

    /**
     * Get a list of all currently known quizzes, with a property marking
     * their availability to fill out.
     *
     * @throws \Exception
     * @return iterable of QuizModel[]
     */
    public function getAvailableQuizzes()
    {
        $quizzes = [];
        foreach ($this->repo->all(QuizModel::query([])) as $quiz) {
            $quizArray = $quiz->jsonSerialize();
            try {
                $quizArray['is_available'] = !$this->isQuizCompleted($quiz->id);
            } catch (ItemNotFoundException $e) {
                // if there's no ongoing session, all quizzes are available!
                $quizArray['is_available'] = true;
            }
            $quizzes[] = $quizArray;
        }
        return $quizzes;
    }

    /**
     * Fetch the given question from the database.
     *
     * @throws ItemNotFoundException if the given ID doesn't correspond to a
     * question
     * @param int $questionId
     * @return QuestionModel
     */
    public function getQuestion(int $questionId): QuestionModel
    {
        $question = $this->repo->get(
            QuestionModel::query(['id' => $questionId]));
        if ($question === null) {
            throw new ItemNotFoundException("Question not found");
        }
        return $question;
    }

    /**
     * Get all questions for a given quiz.
     *
     * @throws \Exception
     * @param int $quizId
     * @return iterable of QuestionModel[]
     */
    public function getQuestions(int $quizId)
    {
        return $this->repo->all(
            QuestionModel::query(['quiz_id' => $quizId]));
    }

    /**
     * Get a list of all stored possible answers to the given question.
     *
     * @throws \Exception
     * @param int $questionId
     * @return iterable of AnswerModel[]
     */
    public function getAnswers(int $questionId)
    {
        return $this->repo->all(
            AnswerModel::query(['question_id' => $questionId]));
    }

    /**
     * Check whether the quiz being filled out during the current session
     * is completed.
     *
     * @throws ItemNotFoundException if there is no ongoing session
     * @param int $quizId
     * @return bool
     */
    public function isQuizCompleted(int $quizId): bool
    {
        $user = $this->users->getCurrentUser();
        if ($user === null) {
            throw new ItemNotFoundException("No ongoing session");
        }

        $questions = $this->repo->count(
            QuestionModel::query(['quiz_id' => $quizId]));
        $answers = $this->repo->count(
            UserAnswerModel::query([
                'user_id' => $user->id,
                'quiz_id' => $quizId,
            ]));

        return $questions === $answers;
    }

    /**
     * Calculate the score of the current session, measured as a percentage
     * of correctly answered questions thus far.
     *
     * @throws ItemNotFoundException
     * @param int $quizId
     * @return int the percentage as a 0-100 integer
     */
    public function getScore(int $quizId): int
    {
        [$correct, $total] = $this->getCorrectAndTotalAnswerCount($quizId);
        return (int)round(100 * $correct / $total);
    }

    /**
     * Get the correct answer count and the total submitted answer count
     * for the current session and a given quiz.
     *
     * Returns a two-item array where the first item is the correct answer
     * count thus far as an int, and the second is the count of submissions
     * thus far.
     *
     * @param int $quizId
     * @throws ItemNotFoundException
     * @return int[] [$correct, $total]
     */
    public function getCorrectAndTotalAnswerCount(int $quizId): array
    {
        $userAnswers = $this->getUserAnswers($quizId);

        $correct = 0;
        $total = 0;
        foreach ($userAnswers as $userAnswer) {
            $total += 1;
            if ($userAnswer->answer->isCorrect) {
                $correct += 1;
            }
        }

        return [$correct, $total];
    }

    /**
     * Get all user answer submissions for the current session and a given
     * quiz.
     *
     * @throws ItemNotFoundException if there is no ongoing session
     * @param int $quizId
     * @return iterable of UserAnswerModel[]
     */
    public function getUserAnswers(int $quizId)
    {
        $user = $this->users->getCurrentUser();
        if ($user === null) {
            throw new ItemNotFoundException("No ongoing session");
        }

        return $this->repo->all(UserAnswerModel::query([
            'user_id' => $user->id,
            'quiz_id' => $quizId,
        ]));
    }

    /**
     * Submit the given answer to the given quiz.
     *
     * The submitted (user, quiz, question) pair should be unique; if it is
     * not (i.e. a duplicate submission is attempted), an
     * AccessViolationException will be thrown.
     *
     * @throws ItemNotFoundException if there's no current user
     * @throws AccessViolationException
     * @throws \ReflectionException
     * @param int $quizId
     * @param int $answerId
     * @return UserAnswerModel
     */
    public function submitAnswer(
        int $quizId,
        int $questionId,
        int $answerId
    ): UserAnswerModel {
        $user = $this->users->getCurrentUser();
        if ($user === null) {
            throw new ItemNotFoundException("No ongoing session");
        }

        // This allows for us to validate both:
        // * whether the answer exists at all
        // * whether the answer is to the question presented
        $answerExists = $this->repo->count(AnswerModel::query([
            'id' => $answerId,
            'question_id' => $questionId,
        ])) > 0;
        if (!$answerExists) {
            throw new ItemNotFoundException("Invalid answer");
        }

        $alreadySubmitted = $this->repo->count(UserAnswerModel::query([
            'user_id' => $user->id,
            'question_id' => $questionId,
        ]));
        if ($alreadySubmitted > 0) {
            throw new AccessViolationException("A duplicate " .
                "submission for question {$questionId} was attempted.");
        }

        $question = $this->repo->get(
            QuestionModel::query(['id' => $questionId]));

        $userAnswer = new UserAnswerModel();
        $userAnswer->user = $user;
        $userAnswer->quizId = $question->quiz->id;
        $userAnswer->questionId = $question->id;
        $userAnswer->answerId = $answerId;
        return $this->repo->insert($userAnswer);
    }

    public function finishAttempt(int $quizId): UserResultModel
    {
        $user = $this->users->getCurrentUser();
        if ($user === null) {
            throw new ItemNotFoundException("No ongoing session");
        }

        $score = $this->getScore($quizId);

        $result = new UserResultModel();
        $result->quizId = $quizId;
        $result->userId = $user->id;
        $result->score = $score;
        return $this->repo->insert($result);
    }

    public function clearAttemptIfExists(int $quizId)
    {
        $user = $this->users->getCurrentUser();
        if ($user === null) {
            return;
        }

        $userAnswers = $this->getUserAnswers($quizId);
        foreach ($userAnswers as $userAnswer) {
            $this->repo->delete($userAnswer);
        }
    }
}
