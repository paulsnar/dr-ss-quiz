<?php


namespace Quiz\Services;


use Quiz\Models\UserModel;
use Quiz\Providers\SessionInterface;
use Quiz\Repositories\ObjectRepositoryInterface;

class UserService
{
    /** @var ObjectRepositoryInterface */
    private $repo;

    /** @var SessionInterface */
    private $session;

    /** @var UserModel | null */
    private $currentUser;

    public function __construct(
        ObjectRepositoryInterface $users,
        SessionInterface $session
    ) {
        $this->repo = $users;
        $this->session = $session;
    }

    /**
     * Returns whether the given `$userId` corresponds to a valid user in the
     * database.
     *
     * @throws \Exception
     * @param int $userId
     * @return bool
     */
    public function isExistingUser(int $userId): bool
    {
        return $this->repo->count(UserModel::query(['id' => $userId])) > 0;
    }

    /**
     * Gets the current user from the session.
     *
     * Returns null if there's none.
     *
     * @throws \Exception
     * @return UserModel|null
     */
    public function getCurrentUser(): ?UserModel
    {
        if ($this->currentUser !== null) {
            return $this->currentUser;
        }

        $id = $this->session->get('user_id');
        if ($id === null) {
            return null;
        }

        $user = $this->repo->get(UserModel::query(['id' => $id]));
        if ($user === null) {
            $this->session->delete('user_id');
        }
        $this->currentUser = $user;
        return $user;
    }

    /**
     * Sets the current user to the session.
     *
     * @param UserModel $user
     */
    public function setCurrentUser(UserModel $user)
    {
        $this->currentUser = $user;
        $this->session->set('user_id', $user->id);
    }

    /**
     * Register the given user into the database.
     *
     * @throws \Exception
     * @param string $name
     * @return UserModel
     */
    public function registerUser(string $name): UserModel
    {
        $user = new UserModel();
        $user->name = $name;
        return $this->repo->insert($user);
    }

    /**
     * Clears the current user from the session.
     */
    public function clearCurrentUser()
    {
        $this->session->delete('user_id');
    }
}
