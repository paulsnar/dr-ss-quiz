<?php

namespace Quiz\Utilities\Structs;

class ImmutableBag extends Bag
{
    public function set(string $key, $value)
    {
        throw new \LogicException('Cannot modify immutable Bag');
    }

    public function unset(string $key)
    {
        throw new \LogicException('Cannot modify immutable Bag');
    }
}
