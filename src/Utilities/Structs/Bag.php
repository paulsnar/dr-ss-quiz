<?php


namespace Quiz\Utilities\Structs;


class Bag
{
    /** @var array */
    protected $values;

    public function __construct(array $values = [])
    {
        $this->values = $values;
    }

    public function contains(string $key)
    {
        return array_key_exists($key, $this->values);
    }

    public function get(string $key)
    {
        return $this->values[$key] ?? null;
    }

    public function set(string $key, $value)
    {
        $this->values[$key] = $value;
    }

    public function unset(string $key)
    {
        if (array_key_exists($key, $this->values)) {
            unset($this->values[$key]);
        }
    }

    public function all()
    {
        foreach ($this->values as $key => $value) {
            yield $key => $value;
        }
    }
}
