<?php


namespace Quiz\Templating;


class Template implements \ArrayAccess
{
    public static $globalContext = [];

    /** @var string */
    private $name;

    /** @var array */
    private $context;

    /** @var Template|null */
    private $parent;

    public function __construct(string $name)
    {
        $this->name = $name;
        $this->context = static::$globalContext;
    }

    private function requestParent(string $name, array $extra = [])
    {
        $this->parent = new Template($name);
        foreach ($extra as $key => $value) {
            $this->parent[$key] = $value;
        }
    }

    private function renderSelf()
    {
        $__filepath = QUIZ_ROOT . "/views/{$this->name}.phtml";

        extract($this->context);
        ob_start();
        try {
            /** @noinspection PhpIncludeInspection */
            include $__filepath;
            return ob_get_contents();
        } finally {
            ob_end_clean();
        }
    }

    public function render()
    {
        $content = $this->renderSelf();
        if ($this->parent === null) {
            return $content;
        }
        $this->parent['content'] = $content;
        return $this->parent->render();
    }

    private function copyUnto(Template $template)
    {
        foreach ($this->context as $key => $value) {
            $template[$key] = $value;
        }
    }

    public function offsetExists($key)
    {
        return array_key_exists($key, $this->context);
    }

    public function offsetGet($key)
    {
        return $this->context[$key] ?? null;
    }

    public function offsetSet($key, $value)
    {
        $this->context[$key] = $value;
    }

    public function offsetUnset($key)
    {
        if ($this->offsetExists($key)) {
            unset($this->context[$key]);
        }
    }
}
