<?php


namespace Quiz\Controllers;


use Quiz\Core\DependencyContainer;
use Quiz\Exceptions\APIException;
use Quiz\HTTP\{Request, Response};

abstract class BaseAPIController extends BaseController
{
    public function invoke(
        DependencyContainer $dc,
        $method,
        Request $request
    ): Response {
        if (!method_exists($this, $method)) {
            return $this->json([
                'ok' => false,
                'error' => 'not_found',
            ], Response::HTTP_NOT_FOUND);
        }

        try {
            $resp = $dc->call($this, $method, $request);
        } catch (APIException $e) {
            $err = [ 'ok' => false, 'error' => $e->kind ];
            if ($e->detail !== null) {
                $err['detail'] = $e->detail;
            }
            $resp = $this->json($err, $e->status);
        } catch (\Throwable $e) {
            $resp = $this->json([
                'ok' => false,
                'error' => 'internal_server_error',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        if (!($resp instanceof Response)) {
            if ($resp instanceof \Iterator) {
                $out = [];
                foreach ($resp as $key => $value) {
                    $out[$key] = $value;
                }
                $resp = $out;
            }
            $resp = $this->json([ 'ok' => true, 'result' => $resp ]);
        }

        if ($request->headers->contains('Origin')) {
            $resp->headers->set('Access-Control-Allow-Origin',
                $request->headers->get('Origin'));
        }

        return $resp;
    }
}
