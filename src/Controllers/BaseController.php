<?php


namespace Quiz\Controllers;


use Quiz\Core\DependencyContainer;
use Quiz\HTTP\{HTTPSerializable, Request, Response};
use Quiz\Templating\Template;

abstract class BaseController
{
    public function json(
        array $values,
        int $status = Response::HTTP_OK,
        array $extraHeaders = []
    ) {
        $headers = [
            'Content-Type' => 'application/json; charset=UTF-8',
        ] + $extraHeaders;

        $json = json_encode($values, \JSON_UNESCAPED_UNICODE | \JSON_UNESCAPED_SLASHES);
        return new Response($status, $headers, $json);
    }

    protected static function failureResponse(): Response
    {
        $message = "<!DOCTYPE html>\n" .
            "<article>Sorry, something went wrong.</article>\n";

        return new Response(Response::HTTP_INTERNAL_SERVER_ERROR, [
            'Content-Type' => 'text/html; charset=UTF-8',
        ], $message);
    }

    public function invoke(
        DependencyContainer $dc,
        $method,
        Request $request
    ): Response {
        if (method_exists($this, 'before')) {
            try {
                $dc->call($this, 'before', $request);
            } catch (\Throwable $e) {
                if ($e instanceof HTTPSerializable) {
                    return $e->serializeHTTP($request);
                }
                return static::failureResponse();
            }
        }

        if (!method_exists($this, $method)) {
            return new Response(Response::HTTP_NOT_FOUND, [
                'Content-Type' => 'text/html; charset=UTF-8',
            ], "<!DOCTYPE html>\n" .
                "<article>Sorry, not found.</article>\n");
        }

        try {
            $resp = $dc->call($this, $method, $request);
        } catch (\Throwable $e) {
            if ($e instanceof HTTPSerializable) {
                return $e->serializeHTTP($e);
            }
            return static::failureResponse();
        }

        if (is_string($resp)) {
            $resp = new Response(Response::HTTP_OK, [
                'Content-Type' => 'text/html; charset=UTF-8',
            ], $resp);
        } else if ($resp instanceof Template) {
            $template = $resp;
            $content = $template->render();
            $resp = new Response(Response::HTTP_OK, [
                'Content-Type' => 'text/html; charset=UTF-8',
            ], $content);
        } else if ($resp instanceof HTTPSerializable) {
            $resp = $resp->serializeHTTP($request);
        }
        return $resp;
    }
}
