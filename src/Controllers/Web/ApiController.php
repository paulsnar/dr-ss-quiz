<?php


namespace Quiz\Controllers\Web;


use Quiz\Controllers\BaseAPIController;
use Quiz\Exceptions\APIException;
use Quiz\HTTP\Request;
use Quiz\Models\{QuestionModel, QuizModel};
use Quiz\Repositories\ObjectRepositoryInterface;
use Quiz\Providers\SessionInterface;
use Quiz\Services\{QuizService, UserService};

class ApiController extends BaseAPIController
{
    public function getIndex()
    {
        return "Hello, world!";
    }

    public function getQuizzes(QuizService $q, Request $rq)
    {
        $quizzes = $q->getAvailableQuizzes();
        return $quizzes;
    }

    public function getQuestions(QuizService $q, Request $rq)
    {
        if ( ! $rq->query->contains('quiz')) {
            throw new APIException('missing_parameter', 'quiz');
        }

        $questionIterator = $q->getQuestions((int) $rq->query->get('quiz'));
        $questions = [];
        foreach ($questionIterator as $question) {
            $questions[] = $question;
        }

        usort($questions, function (QuestionModel $a, QuestionModel $b) {
            return $a->orderWithinQuiz <=> $b->orderWithinQuiz;
        });

        return $questions;
    }

    public function getAnswers(QuizService $q, Request $rq)
    {
        if ( ! $rq->query->contains('question')) {
            throw new APIException('missing_parameter', 'question');
        }

        $answerIterator = $q->getAnswers((int) $rq->query->get('question'));
        $answers = [];
        foreach ($answerIterator as $answer) {
            $answers[] = $answer;
        }

        return $answers;
    }

    public function postStartAttempt(
        ObjectRepositoryInterface $or,
        SessionInterface $session,
        QuizService $quizzes,
        UserService $users,
        Request $rq
    ) {
        $user = $users->getCurrentUser();
        $proposedUserName = $rq->form->get('userName');
        // if we already have an user in the session, update it
        if ($user !== null) {
            if ($proposedUserName !== null) {
                $user->name = $proposedUserName;
                $or->update($user);
                $users->setCurrentUser($user);
            }
        } else {
            // otherwise attempt to create a new one;
            // fail if no name is provided
            if ($proposedUserName === null) {
                throw new APIException('missing_value', 'userName');
            }
            $user = $users->registerUser($rq->form->get('userName'));
            $users->setCurrentUser($user);
        }

        if ( ! $rq->form->contains('quiz')) {
            throw new APIException('missing_value', 'quiz');
        }

        $quizId = (int) $rq->form->get('quiz');
        if ($or->count(QuizModel::query(['id' => $quizId])) === 0) {
            throw new APIException('invalid_value', 'quiz');
        }

        if ($quizzes->isQuizCompleted($quizId)) {
            throw new APIException('quiz_unavailable');
        }

        $quizzes->clearAttemptIfExists($quizId);
        $session->set('quiz_id', $quizId);

        return true;
    }

    public function postFinishAttempt(
        SessionInterface $session,
        QuizService $quizzes,
        UserService $users,
        ObjectRepositoryInterface $or,
        Request $rq
    ) {
        // TODO pn: getCurrentUser should cache result for the request
        if ($users->getCurrentUser() === null) {
            throw new APIException('logged_out');
        }

        $quiz = $session->get('quiz_id');
        if ($quiz === null ||
            $or->count(QuizModel::query(['id' => $quiz])) === 0) {
            throw new APIException('no_ongoing_attempt');
        }

        $submission = $rq->form->get('answers');
        if ($submission === null) {
            throw new APIException('missing_value', 'answers');
        }

        $questions = $or->count(QuestionModel::query(
            [ 'quiz_id' => $quiz ]));
        if ($questions !== count($submission)) {
            throw new APIException('bad_request', 'count_mismatch');
        }

        try {
            foreach ($submission as $question => $answer) {
                $question = (int) $question;
                $answer = (int) $answer;
                $quizzes->submitAnswer($quiz, (int) $question, (int) $answer);
            }
        } catch (ItemNotFoundException $err) {
            throw new APIException('bad_request');
        }

        $result = $quizzes->finishAttempt($quiz);

        return [ 'score' => $result->score ];
    }
}
