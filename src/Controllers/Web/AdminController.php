<?php

namespace Quiz\Controllers\Web;

use Quiz\Core\Configuration;
use Quiz\Controllers\BaseController;
use Quiz\HTTP\{Request, Response};
use Quiz\Exceptions\{HTTPAuthenticationRequiredException, HTTPForbiddenException};
use Quiz\Templating\Template;
use Quiz\Repositories\ObjectRepositoryInterface;
use Quiz\Models\{QuizModel, UserModel, UserResultModel};

class AdminController extends BaseController
{
    public function before(Configuration $conf, Request $rq)
    {
        $auth = $rq->authorization;
        if ($auth === null || is_string($auth)) {
            throw new HTTPAuthenticationRequiredException();
        }

        if ($auth->username !== 'admin' ||
                $auth->password !== $conf->get('admin_password')) {
            throw new HTTPForbiddenException();
        }
    }

    public function getIndex()
    {
        return new Template('admin/index');
    }

    public function getQuiz(ObjectRepositoryInterface $or, Request $rq)
    {
        $quiz = $rq->pathParts[2] ?? '';
        if ($quiz === '') {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $quiz = $or->get(QuizModel::query(['id' => $quiz]));
        if ($quiz === null) {
            return new Response(Response::HTTP_NOT_FOUND);
        }
        return $this->json($quiz->jsonSerialize());
    }

    public function getUser(ObjectRepositoryInterface $or, Request $rq)
    {
        $user = $rq->pathParts[2] ?? '';
        if ($user === '') {
            return new Response(Response::HTTP_BAD_REQUEST);
        }

        $user = $or->get(UserModel::query(['id' => $user]));
        if ($user === null) {
            return new Response(Response::HTTP_NOT_FOUND);
        }
        return $this->json($user->jsonSerialize());
    }

    public function getUserResults(ObjectRepositoryInterface $or)
    {
        $results = [];

        $resultIterator = $or->all(UserResultModel::query([]),
            /* limit */ 30, /* offset */ null,
            /* order */ ['created_at' => 'DESC']);

        /** @var UserResultModel $result */
        foreach ($resultIterator as $result) {
            $results[] = $result;
        }
        return $this->json($results);
    }
}
