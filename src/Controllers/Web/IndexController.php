<?php


namespace Quiz\Controllers\Web;


use Quiz\Controllers\BaseController;
use Quiz\HTTP\Response;


class IndexController extends BaseController
{
    public function getIndex()
    {
        $index = file_get_contents(
            QUIZ_PUBLIC_ROOT . DIRECTORY_SEPARATOR . 'index.html');
        return new Response(200, [
            'Content-Type' => 'text/html; charset=UTF-8',
        ], $index);
    }
}
