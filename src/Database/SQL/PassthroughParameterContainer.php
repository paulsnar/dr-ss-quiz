<?php


namespace Quiz\Database\SQL;


class PassthroughParameterContainer implements SQLParameterContainerInterface
{
    /** @var string */
    public $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getParameter(EscaperInterface $escaper): string
    {
        return $this->value;
    }
}
