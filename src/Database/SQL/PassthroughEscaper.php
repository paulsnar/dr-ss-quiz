<?php


namespace Quiz\Database\SQL;


class PassthroughEscaper implements EscaperInterface
{
    public function identifier($identifier): string
    {
        if ($identifier instanceof SQLSerializableInterface) {
            return $identifier->serialize($this);
        }
        return strval($identifier);
    }

    public function value($value): string
    {
        if ($value instanceof SQLSerializableInterface) {
            return $value->serialize($this);
        }
        return strval($value);
    }
}
