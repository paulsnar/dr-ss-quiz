<?php


namespace Quiz\Database\SQL;


interface EscaperInterface
{
    public function identifier($identifier): string;

    public function value($value): string;
}
