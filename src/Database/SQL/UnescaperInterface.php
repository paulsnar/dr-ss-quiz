<?php


namespace Quiz\Database\SQL;


interface UnescaperInterface
{
    public function unescape(string $value, string $typehint);
}
