<?php


namespace Quiz\Database\SQL;


class MySQLEscaper implements EscaperInterface
{
    const MYSQL_DATETIME_FORMAT = 'Y-m-d H:i:s';

    public function identifier($identifier): string
    {
        if ($identifier instanceof SQLSerializableInterface) {
            return $identifier->serialize($this);
        }
        $identifier = str_replace('`', '``', $identifier);
        return "`{$identifier}`";
    }

    /**
     * Stringify the given value.
     *
     * NOTE: does NOT actually do any escaping! Don't trust user input to this.
     */
    public function value($value): string
    {
        if ($value instanceof SQLSerializableInterface) {
            return $value->serialize($this);
        } else if ($value instanceof \DateTimeInterface) {
            if ($value instanceof \DateTime) {
                $value = \DateTimeImmutable::createFromMutable($value);
            }
            $value = $value->setTimezone(new \DateTimeZone('UTC'));
            return $value->format(static::MYSQL_DATETIME_FORMAT);
        } else if (is_bool($value)) {
            return $value ? '1' : '0';
        } else {
            return strval($value);
        }
    }
}
