<?php


namespace Quiz\Database\SQL\Logic;


use Quiz\Database\SQL\{EscaperInterface, SQLSerializableInterface, SQLParameterContainerInterface};

class WhereClause implements SQLSerializableInterface, SQLParameterContainerInterface
{
    const OPERATOR_EQUALS = '=';
    const OPERATOR_NOT_EQUALS = '!=';
    const OPERATOR_LESS_THAN = '<';
    const OPERATOR_LESS_THAN_EQUALS = '<=';
    const OPERATOR_GREATER_THAN = '>';
    const OPERATOR_GREATER_THAN_EQUALS = '>=';
    const OPERATOR_IN = 'IN';

    /** @var string */
    public $column;

    /** @var mixed */
    public $value;

    /** @var string */
    public $operator;

    /**
     * @param string $column
     * @param mixed $value
     * @param string $operator
     */
    public function __construct(
        string $column,
        $value,
        string $operator = self::OPERATOR_EQUALS
    ) {
        $this->column = $column;
        $this->value = $value;
        $this->operator = $operator;
    }

    public function serialize(EscaperInterface $escaper): string
    {
        $column = $escaper->identifier($this->column);
        return "{$column} {$this->operator} ?";
    }

    public function getParameter(EscaperInterface $escaper): string
    {
        return $escaper->value($this->value);
    }

    public static function equals($column, $value)
    {
        return new static($column, $value, static::OPERATOR_EQUALS);
    }

    public static function notEquals($column, $value)
    {
        return new static($column, $value, static::OPERATOR_NOT_EQUALS);
    }

    public static function lessThan($column, $value, bool $orEquals = false)
    {
        return new static($column, $value,
            $orEquals ?
                static::OPERATOR_LESS_THAN_EQUALS :
                static::OPERATOR_LESS_THAN);
    }

    public static function greaterThan($column, $value, bool $orEquals = false)
    {
        return new static($column, $value,
            $orEquals ?
                static::OPERATOR_GREATER_THAN_EQUALS :
                static::OPERATOR_GREATER_THAN);
    }

    public static function in($column, $value)
    {
        return new static($column, $value, static::OPERATOR_IN);
    }
}
