<?php


namespace Quiz\Database\SQL\Logic;


use Quiz\Database\SQL\{EscaperInterface, SQLParameterCollectorInterface,
    SQLParameterContainerInterface, SQLSerializableInterface};

class ConditionTree implements SQLSerializableInterface, SQLParameterCollectorInterface
{
    const TYPE_AND = 'AND';
    const TYPE_OR = 'OR';

    /** @var string */
    public $type;

    /** @var SQLSerializableInterface[] */
    public $clauses = [];

    public function __construct(string $type)
    {
        if ($type !== static::TYPE_AND &&
            $type !== static::TYPE_OR) {
            throw new \BadMethodCallException(
                "Invalid tree type: {$type}");
        }

        $this->type = $type;
    }

    /** @noinspection PhpDocSignatureInspection */
    /**
     * @var ConditionTree|WhereClause $clause
     */
    public function add(SQLSerializableInterface $clause)
    {
        $this->clauses[] = $clause;
    }

    public function serialize(EscaperInterface $escaper): string
    {
        if (count($this->clauses) === 0) {
            return '';
        }

        $items = array_map(function (SQLSerializableInterface $item) use ($escaper) {
            return $item->serialize($escaper);
        }, $this->clauses);

        $joiner = " {$this->type} ";
        $serialization = implode(" {$this->type} ", $items);
        return "({$serialization})";
    }

    public function collectParameters(EscaperInterface $escaper): array
    {
        $params = [];
        foreach ($this->clauses as $clause) {
            if ($clause instanceof SQLParameterContainerInterface) {
                $params[] = $clause->getParameter($escaper);
            } else if ($clause instanceof SQLParameterCollectorInterface) {
                $params = array_merge(
                    $params, $clause->collectParameters($escaper));
            }
        }

        return $params;
    }

    public static function and(array $clauses): self
    {
        $tree = new static(static::TYPE_AND);
        foreach ($clauses as $key => $value) {
            if (is_string($key)) {
                $tree->add(WhereClause::equals($key, $value));
            } else {
                $tree->add($value);
            }
        }
        return $tree;
    }

    public static function or(array $clauses): self
    {
        $tree = new static(static::TYPE_OR);
        foreach ($clauses as $key => $value) {
            if (is_string($key)) {
                $tree->add(WhereClause::equals($key, $value));
            } else {
                $tree->add($value);
            }
        }
        return $tree;
    }
}
