<?php


namespace Quiz\Database\SQL;


interface SQLParameterCollectorInterface
{
    /** @return string[] */
    public function collectParameters(EscaperInterface $escaper): array;
}
