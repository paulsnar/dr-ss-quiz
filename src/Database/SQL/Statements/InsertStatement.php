<?php


namespace Quiz\Database\SQL\Statements;


use Quiz\Database\SQL\EscaperInterface;
use Quiz\Database\SQL\RawSQLString;
use Quiz\Database\SQL\SQLParameterCollectorInterface;
use Quiz\Database\SQL\SQLSerializableInterface;

class InsertStatement implements SQLSerializableInterface, SQLParameterCollectorInterface
{
    /** @var RawSQLString|string */
    public $table;

    /** @var RawSQLString[]|string[] */
    public $columns = [];

    /** @var array */
    public $values = [];

    public function serialize(EscaperInterface $escaper): string
    {
        $sql = 'INSERT INTO ';
        $sql .= $escaper->identifier($this->table);

        if (count($this->columns) > 0) {
            $columns = array_map(function ($column) use ($escaper) {
                return $escaper->identifier($column);
            }, $this->columns);
            $columns = implode(', ', $columns);
            $sql .= " ({$columns})";
        }

        $sql .= ' VALUES (';
        $values = array_fill(0, count($this->values), '?');
        $sql .= implode(', ', $values) . ')';

        return $sql;
    }

    public function collectParameters(EscaperInterface $escaper): array
    {
        return array_map(function ($value) use ($escaper) {
            return $escaper->value($value);
        }, $this->values);
    }

    // Fluent interface follows
    public static function into(string $table): self
    {
        $insert = new static();
        $insert->table = $table;
        return $insert;
    }
}
