<?php


namespace Quiz\Database\SQL\Statements;


use Quiz\Database\SQL\EscaperInterface;
use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\RawSQLString;
use Quiz\Database\SQL\SQLParameterCollectorInterface;
use Quiz\Database\SQL\SQLSerializableInterface;

class UpdateStatement implements SQLSerializableInterface, SQLParameterCollectorInterface
{
    /** @var RawSQLString|string */
    public $table;

    /** @var RawSQLString[]|string[] */
    public $columns = [];

    /** @var array */
    public $values = [];

    /** @var ConditionTree|null */
    public $where;

    public function serialize(EscaperInterface $escaper): string
    {
        $sql = 'UPDATE ' . $escaper->identifier($this->table) . ' SET ';

        $columns = array_map(function ($column) use ($escaper) {
            $column = $escaper->identifier($column);
            return "$column = ?";
        }, $this->columns);
        $sql .= implode(', ', $columns);

        if ($this->where !== null) {
            $where = $this->where->serialize($escaper);
            if ($where !== '') {
                $sql .= " WHERE {$where}";
            }
        }

        return $sql;
    }

    /** @return string[] */
    public function collectParameters(EscaperInterface $escaper): array
    {
        $values = array_map(function ($value) use ($escaper) {
            return $escaper->value($value);
        }, $this->values);

        if ($this->where === null) {
            return $values;
        }

        return array_merge($values, $this->where->collectParameters($escaper));
    }

    // Fluent interface follows.

    public function __construct($table)
    {
        $this->table = $table;
    }

    public function set($column, $value)
    {
        $this->columns[] = $column;
        $this->values[] = $value;
    }
}
