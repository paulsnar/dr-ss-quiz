<?php


namespace Quiz\Database\SQL\Statements;


use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\RawSQLString;
use Quiz\Database\SQL\SQLParameterCollectorInterface;
use Quiz\Database\SQL\SQLSerializableInterface;
use Quiz\Database\SQL\EscaperInterface;

class SelectStatement implements SQLSerializableInterface, SQLParameterCollectorInterface
{
    /** @var SQLSerializableInterface|RawSQLString[]|string[]|null */
    public $columns;

    /** @var RawSQLString|string|null */
    public $table;

    /** @var ConditionTree|null */
    public $where;

    /** @var int|null */
    public $limit;

    /** @var int|null */
    public $offset;

    /** @var string */
    public $orderBy;

    public function serialize(EscaperInterface $escaper): string
    {
        $sql = 'SELECT';
        if (is_array($this->columns)) {
            if (count($this->columns) === 0) {
                $sql .= ' *';
            } else {
                $columns = [];
                foreach ($this->columns as $key => $value) {
                    if (is_string($key)) {
                        $column = $escaper->identifier($key) . ' AS ' .
                            $escaper->identifier($value);
                        $columns[] = $column;
                    } else {
                        $columns[] = $escaper->identifier($value);
                    }
                }
                $sql .= ' ' . implode(', ', $columns);
            }
        } else if ($this->columns instanceof SQLSerializableInterface) {
            $sql .= ' ' . $this->columns->serialize($escaper);
        }

        if ($this->table !== null) {
            $sql .= ' FROM ';
            $sql .= $escaper->identifier($this->table);
        }

        if ($this->where !== null) {
            $where = $this->where->serialize($escaper);
            if ($where !== '') {
                $sql .= " WHERE {$where}";
            }
        }

        if ($this->orderBy !== null) {
            $orderBy = [];
            foreach ($this->orderBy as $col => $type) {
                if (is_int($col)) {
                    $col = $type;
                    $type = 'ASC';
                }
                $orderBy[] = $escaper->identifier($col) . " {$type}";
            }
            $orderBy = implode(', ', $orderBy);
            $sql .= " ORDER BY {$orderBy}";
        }

        if ($this->limit !== null) {
            $sql .= ' LIMIT ' . strval($this->limit);
        }
        if ($this->offset !== null) {
            $sql .=  ' OFFSET ' . strval($this->offset);
        }

        return $sql;
    }

    public function collectParameters(EscaperInterface $escaper): array
    {
        if ($this->where !== null) {
            return $this->where->collectParameters($escaper);
        }
        return [];
    }

    // Below functions implement a simple fluent interface for building queries.

    public function __construct(...$columns)
    {
        if (is_array($columns[0] ?? null)) {
            $this->columns = $columns[0];
        } else {
            $this->columns = $columns;
        }
    }

    public function from(string $table): self
    {
        $this->table = $table;
        return $this;
    }

    public function where(ConditionTree $where): self
    {
        $this->where = $where;
        return $this;
    }
}
