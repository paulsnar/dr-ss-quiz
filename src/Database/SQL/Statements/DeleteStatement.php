<?php


namespace Quiz\Database\SQL\Statements;


use Quiz\Database\SQL\EscaperInterface;
use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\RawSQLString;
use Quiz\Database\SQL\SQLParameterCollectorInterface;
use Quiz\Database\SQL\SQLSerializableInterface;

class DeleteStatement implements SQLSerializableInterface, SQLParameterCollectorInterface
{
    /** @var RawSQLString|string */
    public $table;

    /** @var ConditionTree|null */
    public $where;

    /** @return string[] */
    public function collectParameters(EscaperInterface $escaper): array
    {
        if ($this->where === null) {
            return [];
        }
        return $this->where->collectParameters($escaper);
    }

    public function serialize(EscaperInterface $escaper): string
    {
        $sql = 'DELETE FROM ' . $escaper->identifier($this->table);
        if ($this->where !== null) {
            $where = $this->where->serialize($escaper);
            if ($where !== '') {
                $sql .= " WHERE {$where}";
            }
        }
        return $sql;
    }

    public function __construct($table)
    {
        $this->table = $table;
    }
}
