<?php


namespace Quiz\Database\SQL;


class MySQLUnescaper implements UnescaperInterface
{
    public function unescape(string $value, string $typehint)
    {
        switch ($typehint) {
            case 'bool':
                return $value === '1';

            case 'int':
                return intval($value, 10);

            case 'float':
                return floatval($value);

            case '\\DateTime': // fallthrough
            case 'DateTime':
                return \DateTime::createFromFormat(
                    MySQLEscaper::MYSQL_DATETIME_FORMAT,
                    $value,
                    new \DateTimeZone('UTC')
                );

            case '\\DateTimeInterface': // fallthrough
            case 'DateTimeInterface':   // fallthrough
            case '\\DateTimeImmutable': // fallthrough
            case 'DateTimeImmutable':
                return \DateTimeImmutable::createFromFormat(
                    MySQLEscaper::MYSQL_DATETIME_FORMAT,
                    $value,
                    new \DateTimeZone('UTC')
                );

            default:
                return $value;
        }
    }
}
