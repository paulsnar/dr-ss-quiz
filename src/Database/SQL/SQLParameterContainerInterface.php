<?php


namespace Quiz\Database\SQL;


interface SQLParameterContainerInterface
{
    public function getParameter(EscaperInterface $escaper): string;
}
