<?php


namespace Quiz\Database\SQL;


class RawSQLString implements SQLSerializableInterface
{
    /** @var string */
    public $value;

    public function __construct(?string $value = null)
    {
        $this->value = $value;
    }

    public function serialize(EscaperInterface $escaper): string
    {
        return $this->value;
    }
}
