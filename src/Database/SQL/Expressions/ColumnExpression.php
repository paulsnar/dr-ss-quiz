<?php


namespace Quiz\Database\SQL\Expressions;


use Quiz\Database\SQL\EscaperInterface;
use Quiz\Database\SQL\SQLSerializableInterface;

class ColumnExpression implements SQLSerializableInterface
{
    /** @var string|SQLSerializableInterface */
    public $column;

    /** @var string|null */
    public $alias;

    public function __construct($column)
    {
        $this->column = $column;
    }

    public function serialize(EscaperInterface $escaper): string
    {
        if (is_string($this->column)) {
            $sql = $escaper->identifier($this->column);
        } else {
            $sql = $this->column->serialize($escaper);
        }
        if ($this->alias !== null) {
            $sql .= ' AS ';
            $sql .= $escaper->identifier($this->alias);
        }
        return $sql;
    }

    public static function as($column, string $alias)
    {
        $colstmt = new static($column);
        $colstmt->alias = $alias;
        return $colstmt;
    }
}
