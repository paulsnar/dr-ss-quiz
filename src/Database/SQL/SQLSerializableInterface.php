<?php


namespace Quiz\Database\SQL;


interface SQLSerializableInterface
{
    public function serialize(EscaperInterface $escaper): string;
}
