<?php


namespace Quiz\Database\ORM;


use function Quiz\FunctionalUtilities\camel_case_to_snake_case;

class ModelProperty
{
    const
        KIND_UNKNOWN = 0,
        KIND_SCALAR = 1,
        KIND_DATETIME = 2,
        KIND_REFERENCE = 3;

    /** @var string */
    public $name;

    /** @var int */
    public $kind;

    /** @var string */
    public $type;

    /** @var string|null */
    public $column;

    /** @var bool */
    public $shouldSerialize = true;

    public static function fromReflectionProperty(\ReflectionProperty $prop): self
    {
        $modelProperty = new static();
        $modelProperty->name = $prop->getName();
        $modelProperty->parseDocComment($prop->getDocComment());
        return $modelProperty;
    }

    public function parseDocComment(string $doc)
    {
        $this->kind = static::KIND_UNKNOWN;
        $this->type = 'string';

        if (strpos($doc, '@var') !== false) {
            $ok = preg_match(
                '/@var\\s+([\\\\A-Za-z0-9_]+)/',
                $doc,
                $matches
            );

            if ($ok !== 1) {
                throw new \Exception('Docblock parse failed due to ' .
                    'a buggy regex :/');
            }
            $type = $matches[1];

            // normalize type aliases
            if ($type === 'boolean') {
                $type = 'bool';
            } else if ($type === 'integer') {
                $type = 'int';
            } else if ($type === 'double' || $type === 'real') {
                $type = 'float';
            } else if ($type === 'DateTimeInterface' ||
                $type === '\\DateTimeInterface' ||
                $type === 'DateTimeImmutable') {
                $type = '\\DateTimeImmutable';
            } else if ($type === 'DateTime') {
                $type = '\\DateTime';
            }

            switch ($type) {
                case 'int': // fallthrough
                case 'bool': // fallthrough
                case 'float': // fallthrough
                case 'string':
                    $this->kind = static::KIND_SCALAR;
                    break;

                case '\\DateTimeImmutable': // fallthrough
                case '\\DateTime':
                    $this->kind = static::KIND_DATETIME;
                    break;

                default:
                    $this->kind = static::KIND_REFERENCE;
                    break;
            }
            $this->type = $type;
        }

        if (strpos($doc, '@columnName') !== false) {
            $ok = preg_match(
                '/@columnName\\s+([A-Za-z0-9_]+)/',
                $doc,
                $matches
            );

            if ($ok !== 1) {
                throw new \Exception('Docblock parse failed due to ' .
                    'a buggy regex :/');
            }

            $this->column = $matches[1];
        }

        if (strpos($doc, '@hiddenJSON')) {
            $this->shouldSerialize = false;
        }
    }

    public function isReference()
    {
        return $this->kind === static::KIND_REFERENCE;
    }

    /**
     * @param Model $model
     * @throws \Exception
     */
    public function getValue(Model $model)
    {
        $name = $this->name;
        if ($this->isReference()) {
            if (array_key_exists($name, $model->_stubs)) {
                $value = ModelStub::_extractReference($model->_stubs[$name]);
            } else if (array_key_exists($name, $model->_stubReferences)) {
                $value = $model->_stubReferences[$name];
            } else {
                /** @var Model|null $submodel */
                $submodel = $model->$name;
                $keys = $submodel::getPrimaryKey();
                if (count($keys) > 1) {
                    throw new \Exception("Cannot reconcile foreign " .
                        "reference with composite primary key");
                }
                $key = $keys[0];
                $value = $submodel->$key;
            }

            return $value;
        } else {
            return $model->$name;
        }
    }

    public function getColumnName()
    {
        if ($this->column !== null) {
            return $this->column;
        }

        $column = camel_case_to_snake_case($this->name);
        if ($this->isReference()) {
            $column .= '_id';
        }
        return $column;
    }
}
