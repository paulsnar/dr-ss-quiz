<?php


namespace Quiz\Database\ORM;


class ModelCondition
{
    /** @var string */
    public $class;

    /** @var array */
    public $conditions;

    public function __construct(
        ?string $class = null,
        ?array $conditions = null
    ) {
        $this->class = $class;
        $this->conditions = $conditions;
    }
}
