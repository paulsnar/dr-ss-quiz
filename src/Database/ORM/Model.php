<?php


namespace Quiz\Database\ORM;

use function Quiz\FunctionalUtilities\str_ends_with;

/**
 * The base class for ORM-esque models.
 */
abstract class Model implements \JsonSerializable
{
    /**
     * @return bool whether the item corresponds to an entity in the database.
     */
    public function doesExist(): bool
    {
        $primaryKeys = static::getPrimaryKey();
        foreach ($primaryKeys as $key) {
            if ($this->$key === null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check whether the entity is new, i.e., not present in the database.
     *
     * Returns the inverse of ->isExisting. (i.e.
     * `$ent->isNew() === ! $ent->isExisting()`)
     *
     * @return bool
     */
    public function isNew(): bool
    {
        return !$this->doesExist();
    }

    /**
     * Get the primary key used for this model.
     *
     * @return array column names comprising the primary key
     */
    public static function getPrimaryKey(): array
    {
        return ['id'];
    }

    public function jsonSerialize()
    {
        $meta = ModelMetadata::fromModelName(static::class);
        $outParams = [];

        foreach ($meta->properties as $key => $prop) {
            if (!$prop->shouldSerialize) {
                continue;
            }
            $outParams[$prop->getColumnName()] = $prop->getValue($this);
        }

        return $outParams;
    }

    /**
     * Create a bound ModelCondition to be used with a ObjectRepositoryInterface
     * partial query.
     *
     * @param array $conditions
     * @return ModelCondition
     */
    public static function query(array $conditions): ModelCondition
    {
        return new ModelCondition(static::class, $conditions);
    }

    /**
     * @var string[]
     * @hiddenJSON
     */
    public $_stubReferences = [];

    /**
     * Interceptor for foreign object reference stubbing.
     *
     * @param string $name
     * @param $value
     */
    public function __set(string $name, $value)
    {
        if (str_ends_with('Id', $name)) {
            $prop = substr($name, 0, -2);
            $this->_stubReferences[$prop] = $value;
            return;
        }

        throw new \Exception("Tried to set undefined model property {$name}");
    }

    /**
     * @var ModelStub[]
     * @hiddenJSON
     */
    public $_stubs = [];
}
