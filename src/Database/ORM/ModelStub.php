<?php


namespace Quiz\Database\ORM;

use Quiz\Repositories\ObjectRepositoryInterface;

class ModelStub
{
    /** @var ObjectRepositoryInterface */
    private $_repo;

    /** @var ModelCondition */
    private $_condition;

    /** @var Model */
    private $_parent;

    /** @var ModelProperty */
    private $_prop;

    /** @var Model | null */
    private $_materialized;

    public function __construct(
        ObjectRepositoryInterface $repo,
        ModelCondition $condition,
        ModelProperty $prop,
        Model $parent
    ) {
        $this->_repo = $repo;
        $this->_condition = $condition;
        $this->_prop = $prop;
        $this->_parent = $parent;
    }

    public function __get($name)
    {
        if ($this->_materialized === null) {
            $this->_materialize();
        }
        return $this->_materialized->$name;
    }

    public function __set($name, $value)
    {
        if ($this->_materialized === null) {
            $this->_materialize();
        }
        $this->_materialized->$name = $value;
    }

    private function _materialize()
    {
        $this->_materialized = $this->_repo->get($this->_condition);
        $name = $this->_prop->name;
        $this->_parent->$name = $this->_materialized;
    }

    public static function _extractReference(ModelStub $stub)
    {
        $cond = $stub->_condition->conditions;
        if (count($cond) > 1) {
            return null;
        }
        return (int) array_values($cond)[0];
    }
}
