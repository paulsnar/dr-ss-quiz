<?php


namespace Quiz\Database\ORM;


use Quiz\Database\SQL\UnescaperInterface;

class ModelReference
{
    /** @var ModelProperty */
    public $property;

    /** @var string */
    private $referenceValue;

    /** @var ModelMetadata */
    public $submodel;

    /** @var string */
    public $name;

    public function __construct(
        ModelProperty $property,
        string $referenceValue
    ) {
        $this->property = $property;
        $this->referenceValue = $referenceValue;

        $this->submodel = ModelMetadata::fromModelName($property->type);
        $this->name = $property->name;
    }

    public function asCondition(): ModelCondition
    {
        $primaryKey = ([ $this->submodel->className, 'getPrimaryKey' ])();

        if (count($primaryKey) > 1) {
            throw new \Exception("Cannot serialize relation linearly");
        }

        $key = $primaryKey[0];
        $keyProp = $this->submodel->properties[$key];
        return new ModelCondition($this->submodel->className, [
            $keyProp->getColumnName() => $this->referenceValue,
        ]);
    }

    public function install(
        UnescaperInterface $unescaper,
        Model $parent,
        array $row
    ) {
        $prop = $this->property->name;
        $submodel = $this->submodel->loadFromRow($unescaper, $row);
        $parent->$prop = $submodel;
        return $submodel;
    }
}
