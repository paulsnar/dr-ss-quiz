<?php


namespace Quiz\Database\ORM;


class IncompleteObjectException extends \Exception
{
    /** @var ModelMetadata */
    public $model;

    /** @var Model */
    public $instance;

    /** @var ModelReference[] */
    public $references;

    public function __construct(
        ModelMetadata $model,
        Model $instance,
        array $references
    ) {
        $this->model = $model;
        $this->instance = $instance;
        $this->references = $references;
    }
}
