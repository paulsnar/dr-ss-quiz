<?php


namespace Quiz\Database\ORM;


use Quiz\Database\SQL\UnescaperInterface;
use function Quiz\FunctionalUtilities\{camel_case_to_snake_case,
    pluralize, snake_case_to_camel_case, str_starts_with, str_ends_with};

class ModelMetadata
{
    /** @var string */
    public $className;

    /** @var string */
    public $table;

    /** @var ModelProperty[] */
    public $properties = [];

    public function __construct(string $className)
    {
        $this->className = $className;
    }

    public static function fromModelName(string $className): self
    {
        $meta = new static($className);

        $class = new \ReflectionClass($className);

        if ($class->hasConstant('TABLE')) {
            $meta->table = $class->getConstant('TABLE');
        } else if ($class->hasMethod('getTable')) {
            if (!$class->getMethod('getTable')->isStatic()) {
                throw new \Exception("Contract violation: " .
                    "class defines non-static getTable method");
            }

            $meta->table = $className::getTable();
        } else {
            $meta->table = static::parseTableNameFromModel($className);
        }

        $props = $class->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach ($props as $prop) {
            if (str_starts_with('_', $prop->name)) {
                continue;
            }
            $property = ModelProperty::fromReflectionProperty($prop);
            $meta->properties[$property->name] = $property;
        }

        return $meta;
    }

    public static function parseTableNameFromModel(string $cls)
    {
        $namespacePath = explode('\\', $cls);
        $localName = array_pop($namespacePath);

        if (!str_ends_with('Model', $localName)) {
            throw new \Exception("Contract violation: " .
                "model name does not have Model suffix");
        }

        $objectName = substr($localName, 0, -5);
        $objectName = lcfirst($objectName);

        $tableName = camel_case_to_snake_case($objectName);
        return pluralize($tableName);
    }

    public function instantiate()
    {
        return new $this->className;
    }

    public function loadFromRow(UnescaperInterface $unescaper, array $row)
    {
        $columnPropertyMap = [];
        foreach ($this->properties as $propName => $prop) {
            $columnPropertyMap[$prop->getColumnName()] = $propName;
        }

        $instance = $this->instantiate();

        $references = [];
        foreach ($row as $column => $value) {
            $propName = $columnPropertyMap[$column];
            $property = $this->properties[$propName];
            if ($property->isReference()) {
                $references[] = new ModelReference($property, $value);
            } else {
                $instance->$propName =
                    $unescaper->unescape($value, $property->type);
            }
        }

        if (count($references) > 0) {
            throw new IncompleteObjectException($this, $instance, $references);
        }

        return $instance;
    }

    public function getProperty(string $column)
    {
        if (substr($column, -3) === '_id') {
            $column = substr($column, 0, -3);
        }
        $name = snake_case_to_camel_case($column);
        return $this->properties[$name] ?? null;
    }
}
