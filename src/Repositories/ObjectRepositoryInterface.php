<?php


namespace Quiz\Repositories;


use Quiz\Database\ORM\Model;
use Quiz\Database\ORM\ModelCondition;

interface ObjectRepositoryInterface
{
    /**
     * Count how many rows match the given conditions.
     *
     * @throws \Exception
     * @param ModelCondition $query
     * @return int
     */
    public function count(ModelCondition $query): int;

    /**
     * Get a single (or first) entity from the object storage where all of the
     * given property definitions match (an AND selection).
     *
     * Returns null if no entity matches the specified conditions.
     *
     * @throws \Exception
     * @param ModelCondition $query
     * @return null|Model
     */
    public function get(ModelCondition $query): ?Model;

    /**
     * Get an iterator to fetch all rows matching the given conditions.
     *
     * @see ObjectRepositoryInterface::get
     * @throws \Exception
     * @param ModelCondition $query
     * @param int|null $limit
     * @param int|null $offset
     * @param array|null $orderBy
     * @return iterable
     */
    public function all(
        ModelCondition $query,
        ?int $limit = null,
        ?int $offset = null,
        ?array $orderBy = null
    );

    /**
     * Insert the given entity into the object storage.
     *
     * Will mutate the provided model, setting its ID.
     *
     * @throws \Exception
     * @param Model $model
     * @return Model
     */
    public function insert(Model $model): Model;

    /**
     * Persist all of the properties of the given entity into the object
     * storage.
     *
     * The entity lookup will be performed using its ID. Foreign references
     * are only updated to keep the ID in check, but updating a parent object
     * won't trigger updates for all its references – that has to be done
     * manually.
     *
     * @throws \LogicException if the model doesn't have an ID defined
     * @throws \Exception
     * @param Model $model
     * @return Model
     */
    public function update(Model $model): Model;

    /**
     * Delete the given entity from the object storage.
     *
     * @throws \LogicException if the model doesn't have an ID defined
     * @throws \Exception
     * @param Model $model
     * @return void
     */
    public function delete(Model $model);
}
