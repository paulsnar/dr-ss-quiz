<?php


namespace Quiz\Repositories;


use Quiz\Database\ORM\{IncompleteObjectException, ModelCondition, ModelMetadata, ModelProperty};
use Quiz\Database\ORM\{Model, ModelStub};
use Quiz\Database\SQL\{EscaperInterface, UnescaperInterface, RawSQLString};
use Quiz\Database\SQL\Expressions\ColumnExpression;
use Quiz\Database\SQL\Logic\ConditionTree;
use Quiz\Database\SQL\Statements\{DeleteStatement, InsertStatement, SelectStatement, UpdateStatement};
use Quiz\Providers\DatabaseConnectionInterface;

class DatabaseObjectRepository implements ObjectRepositoryInterface
{
    /** @var DatabaseConnectionInterface */
    protected $connection;

    /** @var EscaperInterface */
    protected $escaper;

    /** @var UnescaperInterface */
    protected $unescaper;

    private $modelCache = [];

    /**
     * @param DatabaseConnectionInterface $connection
     * @param EscaperInterface $escaper
     * @param UnescaperInterface $unescaper
     */
    public function __construct(
        DatabaseConnectionInterface $connection,
        EscaperInterface $escaper,
        UnescaperInterface $unescaper
    ) {
        $this->connection = $connection;
        $this->escaper = $escaper;
        $this->unescaper = $unescaper;
    }

    protected function buildSelect(
        ModelMetadata $model,
        array $conditions
    ): SelectStatement {
        $columns = array_map(function (ModelProperty $prop) {
            return $prop->getColumnName();
        }, $model->properties);
        $columns = array_values($columns);

        return (new SelectStatement(...$columns))
            ->from($model->table)
            ->where(ConditionTree::and($conditions));
    }

    /**
     * @param ModelMetadata $meta
     * @param array $row
     * @return mixed|Model
     * @throws \Exception
     */
    protected function loadFromRow(ModelMetadata $meta, array $row)
    {
        try {
            $model = $meta->loadFromRow($this->unescaper, $row);
        } catch (IncompleteObjectException $err) {
            $model = $err->instance;
            $references = $err->references;
            foreach ($references as $reference) {
                $name = $reference->name;
                $submodel = $this->getFromCache(
                    $reference->submodel, $reference->asCondition());
                if ($submodel !== null) {
                    $model->$name = $submodel;
                } else {
                    $stub = new ModelStub($this,
                        $reference->asCondition(),
                        $reference->property,
                        $model);
                    $model->_stubs[$reference->name] = $stub;
                    $model->$name = $stub;
                }
            }
        }

        if ($this->modelCache !== null) {
            $cls = $meta->className;
            if ( ! array_key_exists($cls, $this->modelCache)) {
                $this->modelCache[$cls] = [];
            }

            $id = ([ $cls, 'getPrimaryKey' ])();
            if (count($id) === 1) {
                $id = $id[0];
                $this->modelCache[$cls][$model->$id] = $model;
            }
        }

        return $model;
    }

    protected function getFromCache(
        ModelMetadata $meta,
        ModelCondition $condition
    ): ?Model {
        if (count($this->modelCache) === 0) {
            return null;
        }

        if (!array_key_exists($condition->class, $this->modelCache)) {
            return null;
        }

        $cachedItems = $this->modelCache[$condition->class];
        $key = ([ $condition->class, 'getPrimaryKey' ])();
        if (count($key) > 1) {
            return null;
        }

        $key = $key[0];
        $keyProp = $meta->properties[$key] ?? null;
        if ($keyProp === null) {
            return null;
        }

        $column = $keyProp->getColumnName();
        if (!array_key_exists($column, $condition->conditions)) {
            return null;
        }

        $value = $condition->conditions[$column];
        if (!array_key_exists($value, $cachedItems)) {
            return null;
        }

        return $cachedItems[$value];
    }

    /** @inheritdoc */
    public function count(ModelCondition $condition): int
    {
        $meta = ModelMetadata::fromModelName($condition->class);

        $select = (new SelectStatement(
            ColumnExpression::as(new RawSQLString('count(1)'), 'exists')))
            ->from($meta->table)
            ->where(ConditionTree::and($condition->conditions));

        $sql = $select->serialize($this->escaper);
        $params = $select->collectParameters($this->escaper);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);
        $row = $stmt->fetch();
        return intval($row['exists'], 10);
    }

    /** @inheritdoc */
    public function get(ModelCondition $condition): ?Model
    {
        $meta = ModelMetadata::fromModelName($condition->class);

        $cached = $this->getFromCache($meta, $condition);
        if ($cached !== null) {
            return $cached;
        }

        $select = $this->buildSelect($meta, $condition->conditions);
        $select->limit = 1;

        $sql = $select->serialize($this->escaper);
        $params = $select->collectParameters($this->escaper);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);

        $row = $stmt->fetch();
        if ($row === null) {
            return null;
        }

        $model = $this->loadFromRow($meta, $row);

        return $model;
    }

    /** @inheritdoc */
    public function all(
        ModelCondition $condition,
        ?int $limit = null,
        ?int $offset = null,
        ?array $orderBy = null
    ) {
        $meta = ModelMetadata::fromModelName($condition->class);

        $select = $this->buildSelect($meta, $condition->conditions);
        if ($limit !== null) {
            $select->limit = $limit;
        }
        if ($offset !== null) {
            $select->offset = $offset;
        }
        if ($orderBy !== null) {
            $select->orderBy = $orderBy;
        }

        $sql = $select->serialize($this->escaper);
        $params = $select->collectParameters($this->escaper);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);

        $iterator = function () use ($stmt, $meta) {
            while (($row = $stmt->fetch()) !== null) {
                yield $this->loadFromRow($meta, $row);
            }
        };

        return $iterator();
    }

    /** @inheritdoc */
    public function insert(Model $model): Model
    {
        $cls = get_class($model);
        $meta = ModelMetadata::fromModelName($cls);

        $insert = InsertStatement::into($meta->table);

        $insert->columns = [];
        $insert->values = [];
        foreach ($meta->properties as $property) {
            $value = $property->getValue($model);
            if ($value === null) {
                continue;
            }
            $insert->columns[] = $property->getColumnName();
            $insert->values[] = $value;
        }

        $sql = $insert->serialize($this->escaper);
        $params = $insert->collectParameters($this->escaper);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);

        $key = $model::getPrimaryKey();

        // I don't believe this is well defined for multi-key primary keys
        // so we skip that case instead
        if (count($key) === 1) {
            $key = $key[0];
            $model->$key = $this->connection->lastInsertId();
        }
        return $model;
    }

    /** @inheritdoc */
    public function update(Model $model): Model
    {
        if ($model->isNew()) {
            throw new \LogicException("Cannot update unsaved model");
        }
        $cls = get_class($model);
        $meta = ModelMetadata::fromModelName($cls);

        $update = new UpdateStatement($meta->table);
        $keys = $model::getPrimaryKey();

        foreach ($meta->properties as $property) {
            $name = $property->name;
            if (in_array($name, $keys)) {
                continue;
            }
            $update->set($property->getColumnName(), $model->$name);
        }

        $where = [];
        foreach ($keys as $key) {
            $prop = $meta->properties[$key];
            $where[$prop->getColumnName()] = $prop->getValue($model);
        }
        $update->where = ConditionTree::and($where);

        $sql = $update->serialize($this->escaper);
        $params = $update->collectParameters($this->escaper);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);

        return $model;
    }

    /** @inheritdoc */
    public function delete(Model $model)
    {
        if (!$model->doesExist()) {
            throw new \LogicException("Cannot delete unsaved model");
        }
        $cls = get_class($model);
        $meta = ModelMetadata::fromModelName($cls);

        $delete = new DeleteStatement($meta->table);

        $keys = $model::getPrimaryKey();

        $where = [];
        foreach ($keys as $key) {
            $prop = $meta->properties[$key];
            $where[$prop->getColumnName()] = $prop->getValue($model);
        }
        $delete->where = ConditionTree::and($where);

        $sql = $delete->serialize($this->escaper);
        $params = $delete->collectParameters($this->escaper);

        $stmt = $this->connection->prepare($sql);
        $stmt->execute($params);

        // TODO: should probably instead implement an attribute on the model
        // itself to decide whether it is materialized
        foreach ($keys as $key) {
            $model->$key = null;
        }
    }
}
