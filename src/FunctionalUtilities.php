<?php

namespace Quiz\FunctionalUtilities;

// A collection of utility functions.

/**
 * Similar to array_filter, but doesn't preserve keys on the final array.
 *
 * @param array $subject
 * @param callable $predicate A callable of form ($value, $key): bool.
 * If it returns true, the item will be present in the resulting array.
 * @return array
 */
function vector_filter(array $subject, callable $predicate): array
{
    $out = [];
    foreach ($subject as $key => $value) {
        if ($predicate($value, $key)) {
            $out[] = $value;
        }
    }
    return $out;
}

/**
 * Pick a random value out of the array.
 *
 * Like array_rand from stdlib, but returns the value instead of the key.
 *
 * @param array $array
 * @return mixed
 */
function array_pick(array $array)
{
    return $array[array_rand($array)];
}

/**
 * Transform a snake_case string to camelCase.
 *
 * @param string $snakeCased snake cased string
 * @return string camel cased string
 */
function snake_case_to_camel_case(string $snakeCased): string
{
    $parts = explode('_', $snakeCased);
    $out = array_shift($parts);
    $out .= implode('', array_map(function ($part) {
        return ucfirst($part);
    }, $parts));
    return $out;
}

/**
 * Transform a camelCase string to snake_case.
 *
 * @param string $camelCased camel cased string
 * @return string snake cased string
 */
function camel_case_to_snake_case(string $camelCased): string
{
    if (strlen($camelCased) < 1) {
        return '';
    }
    $parts = [];
    $len = strlen($camelCased);
    $part = $camelCased[0];
    for ($i = 1; $i < $len; $i += 1) {
        if (ctype_upper($camelCased[$i])) {
            $parts[] = $part;
            $part = strtolower($camelCased[$i]);
        } else {
            $part .= $camelCased[$i];
        }
    }
    $parts[] = $part;
    return implode('_', $parts);
}

// So much dimmer than Rails' inflector. But works well enough for now!
function pluralize(string $singular): string
{
    if (substr($singular, -1) === 'z') {
        return "{$singular}zes";
    }

    return "{$singular}s";
}

/**
 * Execute `$callback` `$n` times.
 *
 * Equivalent to Ruby's Number::times. Will pass the current iteration as an
 * argument.
 *
 * @param int $n
 * @param callable $callback
 */
function times(int $n, callable $callback)
{
    for ($i = 0; $i < $n; $i += 1) {
        $callback($i);
    }
}

/**
 * Check whether `$haystack` starts with `$needle`.
 *
 * @param string $needle
 * @param string $haystack
 * @return bool
 */
function str_starts_with(string $needle, string $haystack)
{
    return substr($haystack, 0, strlen($needle)) === $needle;
}

/**
 * Check whether `$haystack` ends with `$needle`.
 *
 * @param string $needle
 * @param string $haystack
 * @return bool
 */
function str_ends_with(string $needle, string $haystack)
{
    return substr($haystack, -1 * strlen($needle)) === $needle;
}
