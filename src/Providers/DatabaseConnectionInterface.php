<?php


namespace Quiz\Providers;

/**
 * An interface for a database connection.
 *
 * Loosely (tightly?) mimics PDO.
 */
interface DatabaseConnectionInterface
{
    /**
     * Returns the ID of last INSERT statement, if any.
     *
     * @return int
     */
    public function lastInsertId(): int;

    /**
     * Prepare a statement.
     *
     * @param string $statement
     * @return DatabaseStatementInterface
     */
    public function prepare(string $statement): DatabaseStatementInterface;
}
