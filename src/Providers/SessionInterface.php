<?php


namespace Quiz\Providers;


interface SessionInterface
{
    /**
     * Get the stored value at the given key, if any, or NULL.
     *
     * @param string $name
     * @return null|string
     */
    public function get(string $name): ?string;

    /**
     * Set the given value to be obtainable in the session at the given key.
     *
     * @param string $name
     * @param string $value
     * @return void
     */
    public function set(string $name, string $value);

    /**
     * Delete the given item from the session.
     *
     * @param string $name
     * @return void
     */
    public function delete(string $name);

    /**
     * Delete all items from the session.
     *
     * @return void
     */
    public function reset();
}
