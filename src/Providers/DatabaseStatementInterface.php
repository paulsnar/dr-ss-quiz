<?php


namespace Quiz\Providers;


interface DatabaseStatementInterface
{
    /**
     * Execute this statement.
     *
     * @param array $params
     * @return void
     */
    public function execute(array $params = []): void;

    /**
     * Fetch a row from this statement, if any.
     *
     * @return array|null
     */
    public function fetch(): ?array;
}
