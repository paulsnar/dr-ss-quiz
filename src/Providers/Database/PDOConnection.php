<?php


namespace Quiz\Providers\Database;


use Quiz\Exceptions\PDOException;
use Quiz\Providers\Database\Configuration\PDOConfiguration;
use Quiz\Providers\DatabaseConnectionInterface;
use Quiz\Providers\DatabaseStatementInterface;

class PDOConnection implements DatabaseConnectionInterface
{
    /** @var \PDO */
    protected $connection;

    /** @var PDOConfiguration */
    protected $config;

    public function __construct(PDOConfiguration $config)
    {
        $this->config = $config;
    }

    protected function getConnection()
    {
        if ($this->connection === null) {
            $this->connection = new \PDO(
                $this->config->getDSN(),
                $this->config->username,
                $this->config->password
            );
        }
        return $this->connection;
    }

    /** @inheritdoc */
    public function lastInsertId(): int
    {
        return $this->getConnection()->lastInsertId();
    }

    /**
     * @inheritdoc
     * @throws PDOException
     */
    public function prepare(string $statementString): DatabaseStatementInterface
    {
        $conn = $this->getConnection();
        $statement = $conn->prepare($statementString);
        if ($statement === false) {
            throw new PDOException($conn->errorInfo());
        }

        return new PDOStatement($this, $statement);
    }
}
