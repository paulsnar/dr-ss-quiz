<?php


namespace Quiz\Providers\Database\Configuration;


abstract class PDOConfiguration
{
    /** @var string */
    public $driver, $username, $password;

    abstract public function getDSN();
}
