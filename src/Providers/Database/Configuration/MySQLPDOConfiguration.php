<?php


namespace Quiz\Providers\Database\Configuration;


use Quiz\Core\Configuration;

class MySQLPDOConfiguration extends PDOConfiguration
{
    const DEFAULT_PORT = 3306;

    /** @var string */
    public $driver = 'mysql';

    /** @var string */
    public $host = 'localhost';

    /** @var int */
    public $port = self::DEFAULT_PORT;

    /** @var string */
    public $databaseName;

    /**
     * @param Configuration $config
     * @throws \Exception
     */
    public function __construct(Configuration $config)
    {
        $db = $config->get('database');
        if ($db === null) {
            $db = [];
        }

        if (!array_key_exists('name', $db)) {
            throw new \Exception("Missing database configuration " .
                "option: `name`");
        }
        $this->databaseName = $db['name'];

        if (array_key_exists('host', $db)) {
            $this->host = $db['host'];
        }

        if (array_key_exists('port', $db)) {
            $this->port = $db['port'];
        }

        if (array_key_exists('user', $db)) {
            $this->username = $db['user'];
        }

        if (array_key_exists('password', $db)) {
            $this->password = $db['password'];
        }
    }

    public function getDSN()
    {
        $dsn = "{$this->driver}:charset=utf8mb4;dbname={$this->databaseName}";

        if ($this->host !== null) {
            $dsn .= ";host={$this->host}";
        }

        if ($this->port !== static::DEFAULT_PORT) {
            $dsn .= ";port={$this->port}";
        }

        return $dsn;
    }
}
