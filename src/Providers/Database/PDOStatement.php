<?php


namespace Quiz\Providers\Database;


use Quiz\Exceptions\PDOException;
use Quiz\Providers\DatabaseStatementInterface;

class PDOStatement implements DatabaseStatementInterface
{
    /** @var PDOConnection */
    protected $conn;

    /** @var \PDOStatement */
    protected $stmt;

    /** @var int */
    protected $id;

    /**
     * Internal constructor.
     * @param PDOConnection $conn
     * @param \PDOStatement $stmt
     */
    public function __construct(PDOConnection $conn, \PDOStatement $stmt)
    {
        $this->conn = $conn;
        $this->stmt = $stmt;
    }

    /**
     * @inheritdoc
     * @throws PDOException
     */
    public function execute(array $params = []): void
    {
        if ( !($this->stmt->execute($params))) {
            throw new PDOException($this->stmt->errorInfo());
        }
        $this->id = $this->conn->lastInsertId();
    }

    /** @inheritdoc */
    public function fetch(): ?array
    {
        $row = $this->stmt->fetch(\PDO::FETCH_ASSOC);
        if ($row === false) {
            return null;
        }
        return $row;
    }
}
