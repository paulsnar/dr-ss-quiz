<?php


namespace Quiz\Providers\Session;


use Quiz\Providers\SessionInterface;

class NativeSessionProvider implements SessionInterface
{
    public function __construct()
    {
        session_start();
    }

    /** @inheritdoc */
    public function get(string $name): ?string
    {
        return $_SESSION[$name] ?? null;
    }

    /** @inheritdoc */
    public function set(string $name, string $value)
    {
        $_SESSION[$name] = $value;
    }

    /** @inheritdoc */
    public function delete(string $name)
    {
        if (array_key_exists($name, $_SESSION)) {
            unset($_SESSION[$name]);
        }
    }

    /** @inheritdoc */
    public function reset()
    {
        session_destroy();
        session_start();
    }
}
