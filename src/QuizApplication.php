<?php


namespace Quiz;


use Quiz\Core\Configuration;
use Quiz\Core\DependencyContainer;
use Quiz\Database\SQL\{EscaperInterface, MySQLEscaper, MySQLUnescaper, UnescaperInterface};
use Quiz\HTTP\{Request, Response};
use Quiz\Providers\{DatabaseConnectionInterface, SessionInterface};
use Quiz\Providers\Database\Configuration\{MySQLPDOConfiguration, PDOConfiguration};
use Quiz\Providers\Database\PDOConnection;
use Quiz\Providers\Session\NativeSessionProvider;
use Quiz\Repositories\{DatabaseObjectRepository, ObjectRepositoryInterface};
use function Quiz\FunctionalUtilities\snake_case_to_camel_case;

class QuizApplication
{
    /** @var DependencyContainer */
    public $dependencyContainer;

    public function __construct()
    {
        $this->dependencyContainer = new DependencyContainer();
        // $this->dependencyContainer->store($this);
    }

    public function bootstrap(): self
    {
        $this->setErrorHandler();
        $this->loadConfiguration();

        return $this;
    }

    public function loadConfiguration()
    {
        $this->dependencyContainer->call(
            Configuration::class, 'loadFrom',
            QUIZ_ROOT . DIRECTORY_SEPARATOR . 'config.php');
    }

    public function setErrorHandler()
    {
        set_error_handler(function ($severity, $message, $file, $line) {
            throw new \ErrorException(
                $message, 0, $severity, $file, $line);
        });
    }

    public function dispatchRequest()
    {
        $request = Request::fromGlobals();

        $pathParts = explode('/', $request->path);
        array_shift($pathParts); // remove initial empty element
        if ($pathParts[0] === '') {
            // remove empty index element
            // (e.g. '/' => ['', ''] so we drop both)
            array_shift($pathParts);
        }
        if (!array_key_exists(0, $pathParts) || $pathParts[0] === '') {
            $controller = 'index';
        } else {
            $controller = $pathParts[0];
        }
        if (!array_key_exists(1, $pathParts) || $pathParts[1] === '') {
            $actionName = 'index';
        } else {
            $actionName = $pathParts[1];
        }

        [ $ctrl, $action ] = static::mapControllerAndAction(
            $request->method, $controller, $actionName);

        if (!class_exists($ctrl)) {
            [ $ctrl, $action ] = static::mapControllerAndAction(
                $request->method, 'index', $controller);
        }

        try {
            $response = $this->dependencyContainer->call(
                $ctrl, 'invoke', $action, $request);
        } catch (\Throwable $err) {
            $response = new Response(Response::HTTP_INTERNAL_SERVER_ERROR, [
                'Content-Type' => 'text/plain; charset=UTF-8',
            ], 'Sorry, something went (really) wrong.');
        }

        $response->send();
    }

    public static function mapControllerAndAction(
        string $method,
        string $ctrl,
        string $action
    ) {
        $ctrl = snake_case_to_camel_case($ctrl);
        $ctrl = ucfirst($ctrl);
        $ctrl = "\\Quiz\\Controllers\\Web\\{$ctrl}Controller";

        $action = snake_case_to_camel_case($action);
        $action = strtolower($method) . ucfirst($action);

        return [ $ctrl, $action ];
    }
}
