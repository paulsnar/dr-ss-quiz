<?php


namespace Quiz\Exceptions;


class PDOException extends \Exception
{
    public function __construct(array $errorInfo)
    {
        [ $sqlstate, /* $code */, $message ] = $errorInfo;

        parent::__construct("SQLSTATE {$sqlstate}: {$message}");
    }
}
