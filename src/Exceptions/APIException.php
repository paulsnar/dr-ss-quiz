<?php


namespace Quiz\Exceptions;


use Quiz\HTTP\Response;

class APIException extends \Exception
{
    /** @var string */
    public $kind;

    /** @var mixed|null */
    public $detail;

    /** @var int */
    public $status;

    public function __construct(
        string $kind,
        $detail = null,
        int $status = Response::HTTP_BAD_REQUEST
    ) {
        parent::__construct();
        $this->kind = $kind;
        $this->detail = $detail;
        $this->status = $status;
    }
}
