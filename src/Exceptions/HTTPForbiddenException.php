<?php

namespace Quiz\Exceptions;

use Quiz\HTTP\{HTTPSerializable, Request, Response};

class HTTPForbiddenException extends \Exception implements HTTPSerializable
{
    public function serializeHTTP(Request $rq): Response
    {
        return new Response(Response::HTTP_FORBIDDEN, [
            'Content-Type' => 'text/html; charset=UTF-8',
        ], "<!DOCTYPE html>\n<article>Sorry, you cannot do that.</article>\n");
    }
}
