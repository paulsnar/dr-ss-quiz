<?php

namespace Quiz\Exceptions;

use Quiz\HTTP\{HTTPSerializable, Request, Response};

class HTTPAuthenticationRequiredException extends \Exception implements HTTPSerializable
{
    public function serializeHTTP(Request $rq): Response
    {
        $message = "<!DOCTYPE html>\n" .
            "<article>Sorry, but authorization is required to access " .
                "this resource.</article>\n";
        return new Response(Response::HTTP_UNAUTHORIZED, [
            'Content-Type' => 'text/html; charset=UTF-8',
            'WWW-Authenticate' => 'Basic realm="Quiz Admin"',
        ], $message);
    }
}
