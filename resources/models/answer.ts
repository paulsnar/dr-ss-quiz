import _Base from './_base'
import Question from './question'

export type AnswerData = {
  id: number
  question_id: number
  answer: string
}

export default class Answer extends _Base {
  id: number
  questionId: number
  question: Question | null
  answer: string

  constructor({ id, question_id, answer }: AnswerData) {
    super()
    this.id = id
    this.questionId = question_id
    this.question = null
    this.answer = answer
  }
}
