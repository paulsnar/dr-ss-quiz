import _Base from './_base'
import Quiz from './quiz'

export type QuestionData = {
  id: number
  quiz_id: number
  question: string
  order_within_quiz: number
}

export default class Question extends _Base {
  id: number
  quizId: number
  quiz: Quiz | null
  question: string
  orderWithinQuiz: number

  constructor({ id, quiz_id, question, order_within_quiz }: QuestionData) {
    super()
    this.id = id
    this.quizId = quiz_id
    this.quiz = null
    this.question = question
    this.orderWithinQuiz = order_within_quiz
  }
}
