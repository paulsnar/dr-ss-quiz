import _Base from './_base'

export type QuizData = {
  id: number
  name: string
  is_available: boolean
}

export default class Quiz extends _Base {
  id: number
  name: string
  isAvailable: boolean

  constructor({ id, name, is_available }: QuizData) {
    super()
    this.id = id
    this.name = name
    this.isAvailable = is_available
  }
}
