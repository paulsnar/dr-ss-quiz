import './styles/main.scss'

import store from './store'
import MainView from './views/MainView.vue'

new MainView({
  el: document.getElementById('js-app')!,
  store,
})

import * as Actions from './store/actions'
store.dispatch(Actions.LOAD_QUIZZES)
