import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import Mutations from './mutations'
import Actions from './actions'

import { State, createDefault } from './state'

export default new Vuex.Store<State>({
  state: createDefault(),
  actions: Actions,
  mutations: Mutations,
})
