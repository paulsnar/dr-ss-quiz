import Answer from '../models/answer'
import Question from '../models/question'
import Quiz from '../models/quiz'

export type State = {
  isLoading: boolean

  quizzes: Quiz[]
  questions: Question[]
  answers: Answer[]

  userName: Nullable<string>
  selectedQuiz: Nullable<Quiz>
  currentQuestion: Nullable<Question>
  selectedAnswer: Nullable<Answer>
  pickedAnswers: Nullable<Answer[]>
  resultScore: Nullable<number>
}

export const createDefault = (): State => ({
  isLoading: false,

  quizzes: [],
  questions: [],
  answers: [],

  userName: window.sessionStorage.getItem('userName') || null,
  selectedQuiz: null,
  currentQuestion: null,
  selectedAnswer: null,
  pickedAnswers: null,
  resultScore: null,
})
