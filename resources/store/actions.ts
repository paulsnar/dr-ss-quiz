import { ActionTree } from 'vuex'
import { State } from './state'

import * as Mutations from './mutations'

import Session from '../api/session'
import Answers from '../repos/answers'
import Questions from '../repos/questions'
import Quizzes from '../repos/quizzes'

export const LOAD_QUIZZES = 'loadQuizzes'
export const START_ATTEMPT = 'startAttempt'
export const ADVANCE_QUESTION = 'advanceQuestion'

const actions: ActionTree<State, State> = {}

actions[LOAD_QUIZZES] = ({ commit }) => {
  commit(Mutations.START_LOADING)
  Quizzes.getAll().then((quizzes) => {
    commit(Mutations.SET_QUIZZES, quizzes)
  }, (err) => {
    console.error('Failed to load quizzes: %o', err)
    alert('Diemžēl kaut kas nogāja greizi.')
    commit(Mutations.STOP_LOADING)
  })
}

actions[START_ATTEMPT] = ({ state, commit }) => {
  commit(Mutations.START_LOADING)
  Session.start({
    userName: state.userName,
    quiz: state.selectedQuiz!,
  }).then(() => {
    return Questions.loadAllForQuiz(state.selectedQuiz!)
  }).then((questions) => {
    commit(Mutations.SET_QUESTIONS, questions)
    return Answers.loadAllForQuestion(state.currentQuestion!)
  }, (err) => {
    // up until after this promise we can afford to just clear the loading flag;
    // after this one we have to also reset questions and answers
    console.error('Failed to start attempt: %o', err)
    alert('Diemžēl kaut kas nogāja greizi.')
    commit(Mutations.STOP_LOADING)
  }).then((answers) => {
    commit(Mutations.SET_ANSWERS, answers)
  }, (err) => {
    console.error('Failed to load initial questions: %o', err)
    alert('Diemžēl kaut kas nogāja greizi.')
    commit(Mutations.RESET_QUESTIONS)
  })
}

actions[ADVANCE_QUESTION] = ({ state, commit }) => {
  if (state.selectedAnswer === null) {
    throw new Error('Logic failure: tried to advance question without ' +
      'selecting an answer')
  }

  commit(Mutations.START_LOADING)
  commit(Mutations.APPEND_ANSWER, state.selectedAnswer)

  let nextIdx = state.questions.indexOf(state.currentQuestion!) + 1
  if (nextIdx === 0) {
    console.warn('borken: current question not among known questions')
    alert('Ir notikusi iekšēja kļūda. Atvainojamies par sagādātajām neērtībām.')
    commit(Mutations.RESET_QUESTIONS)
    return
  }

  if (nextIdx === state.questions.length) {
    if (state.pickedAnswers!.length !== state.questions.length) {
      console.warn('borken: question/answer count mismatch')
      alert('Ir notikusi iekšēja kļūda. ' +
        'Atvainojamies par sagādātajām neērtībām.')
      commit(Mutations.RESET_QUESTIONS)
      return
    }
    let submission = { } as StringKeyValue
    for (let i = 0; i < state.questions.length; i += 1) {
      let question = state.questions[i],
          answer = state.pickedAnswers![i]
      submission[question.id.toString()] = answer.id.toString()
    }
    Session.finish(submission).then((score) => {
      commit(Mutations.SET_RESULT_SCORE, score)
    }, (err) => {
      console.error('Could not submit final answers: %o', err)
      alert('Diemžēl kaut kas nogāja greizi.')
      commit(Mutations.RESET_QUESTIONS)
    })
  } else {
    let nextQuestion = state.questions[nextIdx]
    commit(Mutations.SET_CURRENT_QUESTION, nextQuestion)
    Answers.loadAllForQuestion(state.currentQuestion!).then((answers) => {
      commit(Mutations.SET_ANSWERS, answers)
    }, (err) => {
      console.error('Failed to load answers for advancing question: %o', err)
      alert('Diemžēl kaut kas nogāja greizi.')
      commit(Mutations.RESET_QUESTIONS)
    })
  }
}

export default actions
