import { MutationTree } from "vuex"
import { State } from "./state"

import Question from '../models/question'

// utility mutations
export const START_LOADING = 'startLoading'
export const STOP_LOADING = 'stopLoading'

// simple mutations (only set respective property)
export const SET_SELECTED_QUIZ = 'setSelectedQuiz'
export const SET_USER_NAME = 'setUserName'
export const SET_CURRENT_QUESTION = 'setCurrentQuestion'
export const SET_SELECTED_ANSWER = 'setSelectedAnswer'
export const APPEND_ANSWER = 'appendAnswer'
export const RESET_QUESTIONS = 'resetQuestions'
export const RESET_FILLOUT_STATE = 'resetFilloutState'

// async callback mutations (set prop + unset loading state)
export const SET_QUIZZES = 'setQuizzes'
export const SET_QUESTIONS = 'setQuestions'
export const SET_ANSWERS = 'setAnswers'
export const SET_RESULT_SCORE = 'setResultScore'

const mutations: MutationTree<State> = { }

// utility mutators
mutations[START_LOADING] = (state) => { state.isLoading = true }
mutations[STOP_LOADING] = (state) => { state.isLoading = false }

// simple mutators
mutations[SET_SELECTED_QUIZ] = (state, quiz) => { state.selectedQuiz = quiz }
mutations[SET_USER_NAME] = (state, name) => {
  // I know this is bad style but I'm in for the hack
  window.sessionStorage.setItem('userName', name)
  state.userName = name
}
mutations[SET_CURRENT_QUESTION] = (state, question) => {
  state.currentQuestion = question
}
mutations[SET_SELECTED_ANSWER] = (state, answer) => {
  state.selectedAnswer = answer
}
mutations[APPEND_ANSWER] = (state, answer) => {
  if (state.pickedAnswers === null) {
    throw new Error('Mutation logic failure: tried to append answer when ' +
      "picked answers array wasn't set")
  }
  state.pickedAnswers.push(answer)
}
mutations[RESET_QUESTIONS] = (state) => {
  state.questions = []
  state.answers = []
  state.currentQuestion = null
  state.selectedAnswer = null
  state.pickedAnswers = []
  state.isLoading = false
}

mutations[RESET_FILLOUT_STATE] = (state) => {
  state.questions = []
  state.selectedQuiz = null
  state.resultScore = null
}

// async callback mutators
mutations[SET_QUIZZES] = (state, quizzes) => {
  state.quizzes = quizzes
  state.isLoading = false
}

mutations[SET_QUESTIONS] = (state, questions: Question[]) => {
  state.questions = questions
  state.currentQuestion = questions[0]
  state.pickedAnswers = []
  // not setting isLoading = false here because we're yet to fetch answers
}

mutations[SET_ANSWERS] = (state, answers) => {
  state.answers = answers
  state.selectedAnswer = null
  state.isLoading = false
}

mutations[SET_RESULT_SCORE] = (state, score) => {
  state.resultScore = score
  state.currentQuestion = null
  state.answers = []
  state.selectedQuiz!.isAvailable = false
  state.selectedQuiz = null
  state.isLoading = false
}

export default mutations
