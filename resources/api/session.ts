import API from '.'

import Quiz from '../models/quiz'

type StartParams = { quiz: Quiz, userName: Nullable<string> }
export default {
  start({ userName, quiz }: StartParams): Promise<boolean> {
    let params: StringKeyValue = { quiz: quiz.id.toString() }
    if (userName !== null) {
      params['userName'] = userName
    }
    return API.post('startAttempt', params)
  },

  finish(submission: StringKeyValue): Promise<number> {
    let data = new FormData()
    let questions = Object.keys(submission)
    for (let i = 0; i < questions.length; i += 1) {
      let question = questions[i],
          answer = submission[question]
      data.append(`answers[${question}]`, submission[question])
    }
    return API.post('finishAttempt', data).then(({ score }) => score)
  },
}
