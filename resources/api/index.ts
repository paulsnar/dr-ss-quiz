const API = {
  URL_BASE: '/api/',

  _request(
      method: string,
      url: string,
      params: StringKeyValue = { },
      data: FormData | StringKeyValue = { },
      customOptions: RequestInit = { }
  ) {
    let options: RequestInit = { credentials: 'same-origin', ...customOptions }

    let paramKeys = Object.keys(params)
    if (paramKeys.length > 0) {
      let queryParts = [ ]
      for (let key of paramKeys) {
        queryParts.push([ encodeURIComponent(key), encodeURIComponent(params[key]) ])
      }
      url += '?' + queryParts.map(q => q.join('=')).join('&')
    }

    if (data instanceof FormData) {
      options.body = data
    } else {
      let dataKeys = Object.keys(data)
      if (dataKeys.length > 0) {
        let formData = new FormData()
        for (let key of dataKeys) {
          formData.append(key, data[key])
        }
        options.body = formData
      }
    }

    options.method = method

    return fetch(url, options)
  },

  request(
      method: string,
      action: string,
      params: StringKeyValue = { },
      data: FormData | StringKeyValue = { },
      customOptions: RequestInit = { }
  ) {
    let url = `${API.URL_BASE}${action}`
    return this._request(method, url, params, data, customOptions)
      .then(resp => resp.json())
      .then(({ ok, result = null, error = null }) => {
        if (ok) {
          return result
        } else {
          throw new Error(`API request failed: ${error}`)
        }
      })
  },

  get(action: string, params: StringKeyValue = { }) {
    return this.request('GET', action, params)
  },

  post(
      action: string,
      data: FormData | StringKeyValue = {},
      query: StringKeyValue = {}
  ) {
    return this.request('POST', action, query, data)
  },
}

export default API
