import API from '../api'
import Question, { QuestionData } from '../models/question'
import Quiz from '../models/quiz'

export default {
  loadAllForQuiz(quiz: Quiz): Promise<Question[]> {
    return API.get('questions', { quiz: quiz.id.toString() })
    .then((questionData: QuestionData[]) => {
      const questions: Question[] = questionData.map((questionData) => {
        const question = new Question(questionData)
        if (question.quizId !== quiz.id) {
          throw new Error('API returned questions for foreign quiz')
        }
        return question
      })
      return questions.filter(q => q !== null)
    })
  }
}
