import API from '../api'
import Quiz, { QuizData } from '../models/quiz'

export default {
  getAll(): Promise<Quiz[]> {
    return API.get('quizzes').then((quizzes: QuizData[]) => {
      return quizzes.map(quizData => new Quiz(quizData))
    })
  }
}
