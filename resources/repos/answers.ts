import API from '../api'
import Answer, { AnswerData } from '../models/answer'
import Question from '../models/question'

export default {
  loadAllForQuestion(question: Question): Promise<Answer[]> {
    return API.get('answers', { question: question.id.toString() })
    .then((answerData: AnswerData[]) => {
      const answers: Answer[] = answerData.map((answerData) => {
        const answer = new Answer(answerData)
        if (answer.questionId !== question.id) {
          throw new Error('API returned answers for foreign question')
        }
        return answer
      })
      return answers
    })
  }
}
