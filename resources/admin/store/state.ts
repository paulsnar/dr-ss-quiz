import { Quiz, User, UserResult } from '../models'

export type State = {
  userResults: UserResult[] | null
  users: { [id: number]: User },
  quizzes: { [id: number]: Quiz },
}

export const createDefaults = (): State => ({
  userResults: null,
  users: { },
  quizzes: { },
})
