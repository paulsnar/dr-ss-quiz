import { ActionTree } from 'vuex'
import { State } from './state'
import API from '../api'
import { UserResult } from '../models'

import * as Mutations from './mutations'

export const LOAD_USER_RESULTS = 'load user results'
export const LOAD_USER = 'load user'
export const LOAD_QUIZ = 'load quiz'

const actions: ActionTree<State, State> = { }
export default actions

actions[LOAD_USER_RESULTS] = ({ state, commit }) => {
  let load: Promise<UserResult[]>
  if (state.userResults === null) {
    load = API.get('/admin/user_results')
  } else {
    // TODO
    // load = API.get(`/admin/user_results?after=${state.userResults[-1].id}`)
    throw new Error('Not implemented :(')
  }

  return load.then((results) => {
    commit(Mutations.APPEND_RESULTS, results)
  })
}

actions[LOAD_USER] = ({ state, commit }, id) => {
  if (id in state.users) {
    // already loading or loaded
    return
  }
  commit(Mutations.SET_USER, { id, user: null })

  return API.get(`/admin/user/${id}`).then((user) => {
    commit(Mutations.SET_USER, { id, user })
  })
}

actions[LOAD_QUIZ] = ({ state, commit }, id) => {
  if (id in state.quizzes) {
    return
  }

  commit(Mutations.SET_QUIZ, { id, quiz: null })

  return API.get(`/admin/quiz/${id}`).then((quiz) => {
    commit(Mutations.SET_QUIZ, { id, quiz })
  })
}
