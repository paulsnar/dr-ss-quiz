import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import actions from './actions'
import mutations from './mutations'
import { State, createDefaults } from './state'

export default new Vuex.Store<State>({
  state: createDefaults(),
  actions,
  mutations,
})
