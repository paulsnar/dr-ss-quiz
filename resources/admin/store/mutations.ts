import Vue from 'vue'
import { MutationTree } from 'vuex'
import { State } from './state'

export const APPEND_RESULTS = 'append results'
export const SET_USER = 'set user'
export const SET_QUIZ = 'set quiz'

const mutations: MutationTree<State> = { }
export default mutations

mutations[APPEND_RESULTS] = (state, results) => {
  if (state.userResults === null) {
    state.userResults = []
  }
  state.userResults = state.userResults.concat(results)
}

mutations[SET_USER] = (state, { id, user }) => {
  Vue.set(state.users, id, user)
}

mutations[SET_QUIZ] = (state, { id, quiz }) => {
  Vue.set(state.quizzes, id, quiz)
}
