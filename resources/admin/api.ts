type StringKeyValue = { [key: string]: string }

const API = {
  request(
      method: string,
      path: string,
      query: StringKeyValue,
      form: StringKeyValue): Promise<any> {

    let config: RequestInit = { method }

    let queryItems = Object.keys(query)
    if (queryItems.length > 0) {
      queryItems = queryItems.map((key) => {
        let value = query[key]
        return [key, value].map(encodeURIComponent).join('=')
      })
      path += '?' + queryItems.join('&')
    }

    let formItems = Object.keys(form)
    if (formItems.length > 0) {
      let formData = new FormData()
      for (let i = 0; i < formItems.length; i += 1) {
        let key = formItems[i],
            value = form[key]
        formData.append(key, value)
      }
      config.body = formData
    }

    return fetch(path, config).then(r => r.json())
  },

  get(path: string, query: StringKeyValue = { }) {
    return this.request('GET', path, query, { })
  },

  post(path: string, form: StringKeyValue, query: StringKeyValue = { }) {
    return this.request('POST', path, query, form)
  },
}
export default API
