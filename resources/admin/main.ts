import './styles/admin.scss'

import Vue from 'vue'
import store from './store'
import router from '.'

new Vue({
  el: '#js-app',
  store,
  router,
  template: '<router-view></router-view>',
})

