export type Quiz = {
  id: number
  name: string
  questions: Question[]
}

export type Question = {
  id: number
  quiz: Quiz
  question: string
  answers: Answer[]
}

export type Answer = {
  id: number
  question: Question
  answer: string
  isCorrect: boolean
}

export type User = {
  id: number
  name: string
}

export type UserAnswer = {
  id: number
  quiz: Quiz
  question: Question
  answer: Answer
}

export type UserResult = {
  quiz: Quiz
  user: User
  score: number
}
