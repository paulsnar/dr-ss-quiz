import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import IndexView from './views/Index.vue'
import UserView from './views/User.vue'

export default new VueRouter({
  routes: [
    { path: '/', component: IndexView },
    { path: '/user/:user', component: UserView },
    { path: '', redirect: '/' },
  ],
})
