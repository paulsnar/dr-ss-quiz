type SelectPredicate<T> = (item: T, index: number, target: Array<T>) => Boolean
export function select<T>(collection: Array<T>, predicate: SelectPredicate<T>): T {
  for (let i = collection.length - 1; i >= 0; i -= 1) {
    if (predicate(collection[i], i, collection)) {
      return collection[i]
    }
  }
  throw new Error("selection failed: predicate never returned true")
}
