default: dist

.PHONY: dist dist-dev clean

clean:
	rm -rf dist/*.css dist/*.js

dist: clean
	NODE_ENV=production yarn build

dist-dev: clean
	yarn build

# vim: set noet:
