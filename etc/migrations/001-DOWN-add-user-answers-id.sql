alter table `user_results`
  drop primary key,
  drop column `id`,
  add primary key (`quiz_id`, `user_id`);
