-- Some notes on deviations from schema recommendations.
-- * All varchars are at the max 255 chars to store the length prefix as one
--   byte because it's better to have the option to expand, even though this
--   would be a pure metadata change.

create table `quizzes` (
  `id` mediumint unsigned primary key not null auto_increment,
  `name` varchar(255) not null
);

create table `questions` (
  `id` integer unsigned primary key not null auto_increment,
  `quiz_id` mediumint unsigned not null,
  `question` varchar(255) not null,
  `order_within_quiz` smallint unsigned not null,
  foreign key (`quiz_id`) references `quizzes` (`id`)
);

create table `answers` (
  `id` integer unsigned primary key not null auto_increment,
  `question_id` integer unsigned not null,
  `answer` varchar(255) not null,
  `is_correct` boolean not null,
  foreign key (`question_id`) references `questions` (`id`)
);

create table `users` (
  `id` integer unsigned primary key not null auto_increment,
  `name` varchar(255) not null,
  `created_at` datetime not null default NOW()
);

create table `user_answers` (
  `id` integer unsigned primary key not null auto_increment,
  `quiz_id` mediumint unsigned not null,
  `question_id` integer unsigned not null,
  `answer_id` integer unsigned not null,
  `user_id` integer unsigned not null,
  `created_at` datetime not null default NOW(),
  foreign key (`quiz_id`) references `quizzes` (`id`),
  foreign key (`question_id`) references `questions` (`id`),
  foreign key (`answer_id`) references `answers` (`id`),
  foreign key (`user_id`) references `users` (`id`)
);
-- This is a bit wasteful however it should serve to enhance query
-- performance for most cases.
create index `user_answers_by_user_on_quiz` on `user_answers`
  (`user_id`, `quiz_id`);
create index `user_answers_by_user_on_question` on `user_answers`
  (`user_id`, `question_id`);

create table `user_results` (
  `quiz_id` mediumint unsigned not null,
  `user_id` integer unsigned not null,
  `score` tinyint not null, -- as a percentage [0, 100]
  `created_at` datetime not null default NOW(),
  primary key (`quiz_id`, `user_id`),
  foreign key (`quiz_id`) references `quizzes` (`id`),
  foreign key (`user_id`) references `users` (`id`)
);
