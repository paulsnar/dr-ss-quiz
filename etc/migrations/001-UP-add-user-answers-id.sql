-- NOTE: first drop the foreign key constraints that bind user_results.quiz_id
-- and user_results.user_id. MySQL can only drop them by name, but the name
-- specifier in the SQL cannot accept variables so I cannot do that here.
-- The constraint names can be found by:
--    select constraint_name, column_name, referenced_table_name
--      from information_schema.key_column_usage
--      where table_name = 'user_results' and
--        referenced_table_name is not null;
-- They are usually named user_results_ibfk_1 and user_results_ibfk_2, but
-- that is fairly brittle and I don't want to rely upon that.

alter table `user_results`
  -- drop foreign key @fk1,
  -- drop foreign key @fk2,
  drop primary key,
  add column `id` integer unsigned not null primary key auto_increment first,
  add foreign key (`quiz_id`) references quizzes (`id`),
  add foreign key (`user_id`) references users (`id`);
