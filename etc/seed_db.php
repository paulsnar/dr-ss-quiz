<?php

// Borrowed from https://gist.github.com/ingus/1b7ac172a61bdff4160d864a9a7fc41a

const THRESHOLD = 100000;

$db = new mysqli('localhost', 'quiz_stress', '', 'quiz_stress2');
$db->query('set sql_log_bin=off'); // might be a performance boost?

$t = microtime(true);
$msglen = 0;

echo 'Seeding';
$db->begin_transaction();
foreach (xrange(0, THRESHOLD) as $i) {
  $db->query("insert into `quizzes` (`name`) values ('quiz {$i}')");
  $quiz = $db->insert_id;

  foreach (xrange(2, rand(2, 21)) as $ii) {
    $db->query("insert into `quiz_questions` (`quiz_id`, `question`, " .
      "`order_within_quiz`) values ({$quiz}, 'Question {$ii}', {$ii})");
    $question = $db->insert_id;

    $answers = rand(4, 10);
    $correct = rand(1, $answers);
    foreach (xrange(1, $answers) as $iii) {
      $db->query("insert into `quiz_answers` (`question_id`, " .
        "`answer`, `is_correct`) values ({$question}, " .
        "'Answer {$iii}', " . ($iii === $correct ? 1 : 0) . ')');
    }
  }
  
  if ($i % 10 === 0) {
    $delta = microtime(true) - $t;
    $msglen_prev = $msglen;
    $msg = sprintf('Seeding: %d/%d %.1f%% (%.1f/sec)',
      $i, THRESHOLD, 100 * $i / THRESHOLD, 100 / $delta);
    $msglen = strlen($msg);
    $msglen_prev -= $msglen;
    echo "\r$msg", $msglen_prev > 0 ? str_repeat(' ', $msglen_prev) : '';
    $msglen = strlen($msg);
    $t = microtime(true);
  }

  if ($i % 1000 === 999) {
    $db->commit();
    $db->begin_transaction();
  }
}
$db->commit();
echo sprintf("\rSeeding: %d/%d 100.0%% (∞/sec)", THRESHOLD, THRESHOLD),
    PHP_EOL, '-> Done! Have a nice day.', PHP_EOL;
exit(1);

function xrange($from, $to)
{
  // I just love me some generators!
  for ($i = $from; $i < $to; $i += 1) {
    yield $i;
  }
}
