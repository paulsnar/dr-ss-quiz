<?php

return [
    'providers' => [
        \Quiz\Providers\SessionInterface::class =>
            \Quiz\Providers\Session\NativeSessionProvider::class,

        \Quiz\Providers\Database\Configuration\PDOConfiguration::class =>
            \Quiz\Providers\Database\Configuration\MySQLPDOConfiguration::class,

        \Quiz\Database\SQL\EscaperInterface::class =>
            \Quiz\Database\SQL\MySQLEscaper::class,

        \Quiz\Database\SQL\UnescaperInterface::class =>
            \Quiz\Database\SQL\MySQLUnescaper::class,

        \Quiz\Providers\DatabaseConnectionInterface::class =>
            \Quiz\Providers\Database\PDOConnection::class,

        \Quiz\Repositories\ObjectRepositoryInterface::class =>
            \Quiz\Repositories\DatabaseObjectRepository::class,
    ],

    'database' => [
        'name' => 'quiz',
        'user' => 'quiz',
    ],

    'admin_password' => 'hackme',
];
