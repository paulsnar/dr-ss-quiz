const webpack = require('webpack'),
      path = require('path')

const MiniCSSExtractPlugin = require('mini-css-extract-plugin')

const mode = process.env.NODE_ENV || 'development'

module.exports = {
  mode,

  entry: {
    main: './resources/main.ts',
    admin: './resources/admin/main.ts',
    // style: './resources/styles/main.scss',
    // index: './resources/index.html',
  },

  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: 'C~[name].js',
  },

  module: {
    rules: [
      { test: /\.vue$/, loader: 'vue-loader',
        options: { loaders: {
          scss: 'vue-style-loader!css-loader!sass-loader',
          sass: 'vue-style-loader!css-loader!sass-loader?indentedSyntax' } } },
      { test: /\.tsx?$/, loader: 'ts-loader', exclude: /node_modules/,
        options: { appendTsSuffixTo: [ /\.vue$/ ] } },
      { test: /\.(png|jpe?g|gif|svg)$/, loader: 'file-loader',
        options: { name: '[name].[ext]?[hash]' } },
      { test: /\.scss$/,
        use: [ MiniCSSExtractPlugin.loader, 'css-loader', 'sass-loader' ] },
      { test: /\.css$/,
        loader: [ MiniCSSExtractPlugin.loader, 'css-loader' ], },
    ],
  },

  resolve: {
    extensions: [ '.ts', '.js', '.vue', '.json' ],
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
    },
  },

  devServer: {
    port: 8081,
    historyApiFallback: true,
    noInfo: true,
    contentBase: path.resolve(__dirname, './dist'),
    proxy: {
      '/api': 'http://quiz.test',
    },
  },


  plugins: [
    new MiniCSSExtractPlugin({
      filename: '[name].css',
      chunkFilename: 'C~[name].css',
    }),
    new (require('vue-loader/lib/plugin'))(),
    new (require('html-webpack-plugin'))({
      template: 'resources/index.html',
      chunks: ['main'],
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify(mode),
      },
    }),
  ],
}

if (mode !== 'production') {
  module.exports.devtool = '#eval-source-map'
}
